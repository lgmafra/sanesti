<?php

use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */

$this->title = 'Home';
?>
<div class="site-index">

    <?= Highcharts::widget([
        'options' => [
           'title' => ['text' => 'Produção por Mês'],
           'xAxis' => [
              'categories' => $model['categorias']
           ],
           'yAxis' => [
               'title' => ['text' => 'Quantidade de Procedimentos']
           ],
           'series' => [
               [
                    'type'=>'column',
                    'name'=>$model['series']['name'][0],
                    'data'=>$model['series']['cirurgias']
               ],
               [
                   'type'=>'column',
                   'name'=>$model['series']['name'][1],
                   'data'=>$model['series']['consultas']
               ],
               [
                    'type'=>'spline',
                    'name'=>$model['spline']['name'],
                    'data'=>$model['spline']['producao'],
                    'marker' => [
                        'lineWidth' => 2,
                        'lineColor' => new JsExpression('Highcharts.getOptions().colors[2]'),
                        'fillColor' => 'red',
                    ],
               ],
           ]
        ]
     ]); ?>

    <?= Highcharts::widget([
        'options' => [
            'title' => ['text' => 'Faturamento por Mês'],
            'xAxis' => [
                'categories' => $model['categorias']
            ],
            'yAxis' => [
                'title' => ['text' => 'Pagamentos']
            ],
//            'tooltip' => [//'formatter' => new JsExpression('function(){ return this.series.name; }')
//                'pointFormat' => new JsExpression('function() { return Highcharts.numberFormat(point.y:.2f, 2, ".", ","); }')
//            ],
            'series' => [
                [
                    'type'=>'column',
                    'name'=>$model['series']['name'][2],
                    'data'=>$model['series']['valorpago']
                ],
            ]
        ]
    ]); ?>
    
</div>
