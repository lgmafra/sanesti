<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
    ]); ?>

        <?= $form->field($model, 'username', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><i class="fa fa-lg fa-user"></i></span></div>',
        ])->textInput(['autofocus' => false]) ?>

        <?= $form->field($model, 'password', [
            'inputTemplate' => '<div class="input-group">{input}<span class="input-group-addon"><i class="fa fa-lg fa-lock"></i></span></div>',
        ])->passwordInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>
