<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoFaturaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="importacao-fatura-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idImportacaoFatura') ?>

    <?= $form->field($model, 'competenciaFatura') ?>

    <?= $form->field($model, 'descricao') ?>

    <?= $form->field($model, 'empresaFatura') ?>

    <?= $form->field($model, 'idUsuario') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
