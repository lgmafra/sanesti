<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ImportacaoFaturaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Importar Fatura';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function() {
    $('#modalImportacao').on('show.bs.modal', function(e) {
        var action = $(e.relatedTarget).data('action');
        var html = $(e.relatedTarget).data('html');
        
        $(".modal-body").html(html);
        
        $("#urlDestino").attr("href", action);
    });
});
JS;

$this->registerJs($script, \yii\web\VIEW::POS_LOAD);
?>
<div class="importacao-fatura-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Importar Fatura', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idImportacaoFatura',
            [
                'attribute'=>'competenciaFatura',
                'filterInputOptions'=>['class'=>'form-control competencia']
            ],
            'descricao',
            [
                'attribute'=>'empresaFatura',
                'value'=>function($data){
                    switch($data->empresaFatura){
                        case "C":
                            return "COOPANEST";
                            break;
                        case "U":
                            return "Unimed";
                            break;
                        case "S-HMN":
                            return "Santa Casa - HMN";
                            break;
                        case "S-HCMF":
                            return "Santa Casa - HCMF";
                            break;
                        case "P":
                            return "Particular";
                            break;
                    }
                },
                'filter'=>false
            ],
            'usuario.nomeUsuario',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete} {reprocessar}',
                'buttons' => [
                    'view' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>', 
                            ['fatura/', 'id'=>$model->idImportacaoFatura], 
                            ['title' => 'Visualizar Faturamento']
                        );
                    },
                    'reprocessar' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-refresh"></span>',
                            //['importacao-fatura/reprocessar/', 'id'=>$model->idImportacaoFatura],
                            //['title' => 'Reprocessar Faturamento'],
                            ['#'],
                            [
                                'title' => 'Reprocessar Faturamento',
                                'data-toggle' => "modal",
                                'data-target' => '#modalImportacao',
                                'data-action' => $url,
                                'data-id' => $model->idImportacaoFatura,
                                'data-html' => "Deseja realmente reprocessar essa fatura?"
                            ]
                        );
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['#'],
                            [
                                'title' => 'Excluir Importação',
                                'data-toggle' => "modal",
                                'data-target' => '#modalImportacao',
                                'data-action' => $url,
                                'data-id' => $model->idImportacaoFatura,
                                'data-html' => "Excluir esta importação, irá remover todos os dados de fatura realacionados a ela.<br><br>Deseja continuar?"
                            ]
                        );
                    }
                ],
                'visibleButtons' => [
                     'delete' => function($model, $key, $index){
                        return $model->empresaFatura == "P" ? false : true;
                     },
                    'reprocessar' => function($model, $key, $index){
                        return $model->empresaFatura == "P" ? false : true;
                    }
                ]
            ],
        ],
    ]); ?>
</div>

<div class="modal fade" id="modalImportacao" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="modalLabel"><strong>Alerta</strong></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
        <div class="modal-body">

        </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
        <a class="btn btn-primary" id="urlDestino" href="#" >Sim</a>
      </div>
    </div>
  </div>
</div>