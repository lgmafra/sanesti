<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoFatura */

$this->title = $model->competenciaFatura;
$this->params['breadcrumbs'][] = ['label' => 'Importar Fatura', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacao-fatura-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->idImportacaoFatura], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->idImportacaoFatura], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idImportacaoFatura',
            'competenciaFatura',
            'descricao',
            'empresaFatura',
            'idUsuario',
        ],
    ]) ?>

</div>
