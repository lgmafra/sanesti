<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoFatura */

$this->title = 'Nova Importação de Fatura';
$this->params['breadcrumbs'][] = ['label' => 'Importar Fatura', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacao-fatura-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
