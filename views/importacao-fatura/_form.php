<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoFatura */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS
$(document).ready(function() {
    if($("input[name*=empresaFatura]").is(":checked")){
        if(empresaFatura == "C"){
            $("#arquivoPgto").show();
        }
        $(".competencia").attr('readonly', false);
    }
    var empresaFatura = "";
    $("#arquivoPgto").hide();
        
    $(".competencia").keyup(function(){
        if($(this).val().length == 7){
            empresa = "";
            
            switch(empresaFatura){
                case "C":
                    empresa = "COOPANEST";
                    break;
                case "S-HCMF":
                    empresa = "Santa Casa - HCMF";
                    break;
                case "S-HMN":
                    empresa = "Santa Casa - HMN";
                    break;
                case "U":
                    empresa = "Unimed";
                    break;
            }
            
            
            $("#importacaofatura-descricao").val("Importação de Faturamento Competência: "+ $(this).val() + " - " + empresa);
        }else if($(this).val().length <= 7){
            $("#importacaofatura-descricao").val("");
        }
    });
    
    $("input[name*=empresaFatura]").click(function(){
        empresaFatura = $(this).val();
        $(".competencia").attr('readonly', false);
        if(empresaFatura == "C" || empresaFatura == 'U'){
            $("#arquivoPgto").show();
        }else{
            $("#arquivoPgto").hide();
        }
        
        switch(empresaFatura){
            case "C":
                empresa = "COOPANEST";
                break;
            case "S-HCMF":
                empresa = "Santa Casa - HCMF";
                break;
            case "S-HMN":
                empresa = "Santa Casa - HMN";
                break;
            case "U":
                empresa = "Unimed";
                break;
        }
        
        if($("#importacaofatura-descricao").val() != ""){
            var msg = $("#importacaofatura-descricao").val().split("-");
            $("#importacaofatura-descricao").val(msg[0] + "- " + empresa);
        }
    });
        
    $("#btnImporta").click(function(){
        if(empresaFatura == 'C' && $("#importacaofatura-dataPagamento").val() == ""){
            alert("Informe a data de pagamento da fatura");
            return false;
        }
    });
});
JS;

$this->registerJs($script, \yii\web\VIEW::POS_LOAD);

?>

<div class="importacao-fatura-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'empresaFatura')->radioList(['C' => 'COOPANEST', 'U' => 'Unimed', 'S-HCMF' => 'Santa Casa ( HCMF )', 'S-HMN' => 'Santa Casa ( HMN )'], ['class'=>'i-checks']) ?>

    <?= $form->field($model, 'competenciaFatura')->textInput(['class'=>'form-control competencia', 'readonly'=>true]) ?>

    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true, 'readonly'=>true]) ?>

    <?php echo $form->field($model, '_arquivoImportacao')->widget(FileInput::classname(), [
        'pluginOptions'=>[
            'showPreview' => false,
            'showRemove' => true,
            'showUpload' => false,
            'showCancel' => false,
            'browseClass' => 'btn btn-success',
            'removeClass' => 'btn btn-danger',
            'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
        ],
    ]);?>
    
    <div id="arquivoPgto">
    <?= $form->field($model, 'dataPagamento')->textInput(['class'=>'form-control data']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id'=>'btnImporta']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
