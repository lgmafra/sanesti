<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoFatura */

$this->title = 'Update Importacao Fatura: ' . $model->idImportacaoFatura;
$this->params['breadcrumbs'][] = ['label' => 'Importacao Faturas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idImportacaoFatura, 'url' => ['view', 'id' => $model->idImportacaoFatura]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="importacao-fatura-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
