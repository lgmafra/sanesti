<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pacientes */

$this->title = 'Atualizar Paciente: '.$model->nomePaciente;
$this->params['breadcrumbs'][] = ['label' => 'Pacientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomePaciente, 'url' => ['view', 'id' => $model->idPacientes]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="pacientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
