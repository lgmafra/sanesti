<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) . " | " . Html::encode(Yii::$app->name)  ?></title>
    <?php $this->head() ?>
</head>

<body class="bg-dark">
  <div class="container">
      <h1 class="text-center mt-3"><span style="color: #fff;">SANESTI</span></h1>
      
    <div class="card card-login mx-auto mt-4">
        <div class="card-header text-center"><h5><i class="fa fa-lg fa-unlock-alt"></i> Faça o seu login</h5></div>
      <div class="card-body">
        
        <?= $content ?>
        
        <div class="text-center">
            <a class="d-block small" href="<?= Yii::$app->urlManager->createUrl(['/site/recuperasenha']) ?>">Esqueceu a senha?</a>
        </div>
      </div>
    </div>
      <h5 class="text-center"><span style="color: #478FCA;">GM SISTEMAS&copy;</span></h5>
  </div>
<?php $this->endBody() ?>
</div>

</body>
</html>
<?php $this->endPage() ?>
