<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?= Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) . " | " . Html::encode(Yii::$app->name)  ?></title>
    <?php $this->head() ?>
</head>
<body class="fixed-nav sticky-footer bg-dark sidenav-toggled" id="page-top">
<?php $this->beginBody() ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="<?= Yii::$app->urlManager->createUrl(['site/index']); ?>"><?= Yii::$app->name ?></a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
	
	<div class="collapse navbar-collapse" id="navbarResponsive">
		<ul class="navbar-nav navbar-sidenav">
			<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
			  <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['site/index']); ?>">
				<i class="fa fa-fw fa-lg fa-home"></i>
				<span class="nav-link-text small">Home</span>
			  </a>
			</li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Prestadores">
			  <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['prestador/']); ?>">
				<i class="fa fa-fw fa-lg fa-user-md"></i>
				<span class="nav-link-text small">Prestadores</span>
			  </a>
			</li>

			<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Usuarios">
              <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl((Yii::$app->user->identity->getIsUserAdmin() ? ['usuario/'] : ['usuario/update', 'id'=>Yii::$app->user->getId()])); ?>">
				<i class="fa fa-fw fa-lg fa-users"></i>
				<span class="nav-link-text small">Usuários</span>
			  </a>
			</li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Pacientes">
                <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['pacientes/']); ?>">
                    <i class="fa fa-fw fa-lg fa-male"></i>
                    <span class="nav-link-text small">Pacientes</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Procedimentos">
                <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['procedimentos/']); ?>">
                    <i class="fa fa-fw fa-lg fa-flask"></i>
                    <span class="nav-link-text small">Procedimentos</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Validar Boletins">
                <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['cirurgias/validaboletins']); ?>">
                    <i class="fa fa-fw fa-lg fa-check-square-o"></i>
                    <span class="nav-link-text small">Procedimentos</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Importar Produção">
                <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['importacao-producao/']); ?>">
                    <i class="fa fa-fw fa-lg fa-file-text"></i>
                    <span class="nav-link-text small">Importar Produção</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Importar Faturamento">
			  <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['importacao-fatura/']); ?>">
				<i class="fa fa-fw fa-lg fa-money"></i>
				<span class="nav-link-text small">Importar Faturamento</span>
			  </a>
			</li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Lançamentos">
			  <a class="nav-link" href="<?= Yii::$app->urlManager->createUrl(['cirurgias/']); ?>">
				<i class="fa fa-fw fa-lg fa-th-list"></i>
				<span class="nav-link-text small">Lançamentos</span>
			  </a>
			</li>
		</ul>
		
		<ul class="navbar-nav sidenav-toggler">
			<li class="nav-item">
			  <a class="nav-link text-center" id="sidenavToggler">
				<i class="fa fa-fw fa-angle-left"></i>
			  </a>
			</li>
		</ul>
		
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
			  <a class="nav-link" data-toggle="modal" data-target="#logoutModal">
                              (<?php echo Yii::$app->user->identity->getNome(); ?>)
				<i class="fa fa-fw fa-sign-out"></i>Logout</a>
			</li>
		</ul>
	</div>
</nav>

<div class="content-wrapper">
    <div class="container-fluid">
        <?= Breadcrumbs::widget([
                'itemTemplate' => "<li class='breadcrumb-item'>{link}</i>",
                'activeItemTemplate' => "<li class='breadcrumb-item'>{link}</i>",
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        
        <?php echo Alert::widget(); ?>
        
        <?= $content ?>
    </div>
	
	<footer class="sticky-footer">
		<div class="container">
			<div class="text-center">
			<small class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></small>
                        <small class="pull-right"><?= Html::encode("Desenvolvido Por: ") ?> <?= Html::mailto("GM Sistemas", "lgmafra@gmail.com") ?></small>
			<!--<small class="pull-right"><?= Yii::powered() ?></small>-->
			</div>
		</div>
	</footer>
	
	<!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Aviso</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Tem certeza que deseja finalizar o sistema?</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="<?php echo Yii::$app->urlManager->createUrl(['/site/logout']); ?>">Sair</a>
          </div>
        </div>
      </div>
    </div>

<?php $this->endBody() ?>
</div>

</body>
</html>
<?php $this->endPage() ?>
