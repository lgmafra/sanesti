<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ImportacaoProducaoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Importar Produção';
$this->params['breadcrumbs'][] = $this->title;

$script = <<< JS
$(document).ready(function() {
    $('#deletaImportacao').on('show.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('id');
        var action = $(e.relatedTarget).data('action');
        $("#urlDestino").attr("href", action);
    });
});
JS;

$this->registerJs($script, \yii\web\VIEW::POS_LOAD);
?>
<div class="importacao-producao-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Importar Produção', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idImportacaoProducao',
            [
                'attribute'=>'competenciaProducao',
                'filterInputOptions'=>['class'=>'form-control competencia']
            ],
            [
                'attribute'=>'dataInicial',
                'filterInputOptions'=>['class'=>'form-control data']
            ],
            [
                'attribute'=>'dataFinal',
                'filterInputOptions'=>['class'=>'form-control data']
            ],
            'descricao',
            [
                'attribute'=>'tipoImportacao',
                'value'=>function($data){
                    if($data->tipoImportacao == "P"){
                        return "Cirurgias";
                    }else if($data->tipoImportacao == "C"){
                        return "Consultas";
                    }
                },
                'filter'=>Html::activeDropDownList($searchModel, 'tipoImportacao', ['P'=>'Cirurgias', 'C'=>'Consultas'], ['class'=>'form-control','prompt' => 'Escolha'])
            ],
            'usuario.nomeUsuario',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>', 
                            ['cirurgias/', 'id'=>$model->idImportacaoProducao], 
                            ['title' => 'Visualizar Cirurgias']
                        );
                    },
                    'update' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-pencil"></span>', 
                            ['importacao-producao/update', 'id'=>$model->idImportacaoProducao], 
                            ['title' => 'Editar Importação']
                        );
                    },
                    'delete' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>', 
                            ['#'], 
                            [
                                'title' => 'Excluir Importação',
                                'data-toggle' => "modal",
                                'data-target' => '#deletaImportacao',
                                'data-action' => $url,
                                'data-id' => $model->idImportacaoProducao
                            ]
                        );
                    }
                ]
            ],
        ],
    ]); ?>
</div>

<div class="modal fade" id="deletaImportacao" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="modalLabel"><strong>Alerta</strong></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
        <div class="modal-body">
            Excluir esta importação, irá remover todos os dados da produção realacionados a ela.<br><br>
            Deseja continuar?
        </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
        <a class="btn btn-primary" id="urlDestino" href="#" >Sim</a>
      </div>
    </div>
  </div>
</div>