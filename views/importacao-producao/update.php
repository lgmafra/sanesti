<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoProducao */

$this->title = 'Atualizar Produção Competência: ' . $model->competenciaProducao;
$this->params['breadcrumbs'][] = ['label' => 'Importacao Producaos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->competenciaProducao, 'url' => ['view', 'id' => $model->idImportacaoProducao]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="importacao-producao-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
