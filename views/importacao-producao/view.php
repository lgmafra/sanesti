<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoProducao */

$this->title = $model->competenciaProducao;
$this->params['breadcrumbs'][] = ['label' => 'Importar Produção', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacao-producao-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!--
        <?= Html::a('Atualizar', ['update', 'id' => $model->idImportacaoProducao], ['class' => 'btn btn-primary']) ?>
        -->
        <?= Html::a('Excluir', ['delete', 'id' => $model->idImportacaoProducao], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idImportacaoProducao',
            'competenciaProducao',
            'dataInicial',
            'dataFinal',
            'descricao',
            'usuario.nomeUsuario',
        ],
    ]) ?>

</div>
