<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoProducao */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS
$(document).ready(function() {
    $(".competencia").on("keyup blur", function(){
        if($(this).val().length == 7){
            comp = $(this).val().split("/");
            var lastDay = (new Date(comp[1], comp[0], 0)).getDate();
        
            var dataInicio = "01/"+$(this).val();
            var dataFinal = lastDay+"/"+$(this).val();
            $("#importacaoproducao-datainicial").val(dataInicio);
            $("#importacaoproducao-datafinal").val(dataFinal);
            $("#importacaoproducao-descricao").val("Importação Produção Período: "+ dataInicio + " - " + dataFinal);
        }else if($(this).val().length <= 7){
            $("#importacaoproducao-datainicial").val("");
            $("#importacaoproducao-datafinal").val("");
            $("#importacaoproducao-descricao").val("");
        }
    });
        
    $(".data").keyup(function(){
        if($(this).val().length == 10){
            var dataInicio = $("#importacaoproducao-datainicial").val();
            var dataFinal = $("#importacaoproducao-datafinal").val();
            $("#importacaoproducao-descricao").val("Importação Produção Período: "+ dataInicio + " - " + dataFinal);
        }else if($(this).val().length == 0){
            $("#importacaoproducao-competenciaproducao").val("");
            $("#importacaoproducao-datainicial").val("");
            $("#importacaoproducao-datafinal").val("");
            $("#importacaoproducao-descricao").val("");
        }
    });
});
JS;

$this->registerJs($script, \yii\web\VIEW::POS_LOAD);

?>

<div class="importacao-producao-form">

    <?php $form = ActiveForm::begin([
        'options'=>[
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-2">
    <?= $form->field($model, 'competenciaProducao')->textInput(['class'=>'form-control competencia']) ?>
        </div>
        <div class="col-md-2">
    <?= $form->field($model, 'dataInicial')->textInput(['class'=>'form-control data', 'readonly'=>true]) ?>
        </div>
        <div class="col-md-2">
    <?= $form->field($model, 'dataFinal')->textInput(['class'=>'form-control data', 'readonly'=>false]) ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'descricao')->textInput(['maxlength' => true, 'readonly'=>true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
    <?= $form->field($model, 'tipoImportacao')->dropDownList(['P'=>'Cirurgias', 'C'=>'Consultas'], ['prompt'=>'Escolha']) ?>
        </div>
        <div class="col-md-9">
    <?php echo $form->field($model, '_arquivoImportacao')->widget(FileInput::classname(), [
        'pluginOptions'=>[
            'showPreview' => false,
            'showRemove' => true,
            'showUpload' => false,
            'showCancel' => false,
            'browseClass' => 'btn btn-success',
            'removeClass' => 'btn btn-danger',
            'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
        ],
    ]);?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
