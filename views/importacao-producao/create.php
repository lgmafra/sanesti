<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoProducao */

$this->title = 'Nova Importação da Produção';
$this->params['breadcrumbs'][] = ['label' => 'Importar Produção', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="importacao-producao-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
