<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Procedimentos */

$this->title = 'Atualizar Procedimento: '.$model->descricaoProcedimento;
$this->params['breadcrumbs'][] = ['label' => 'Procedimentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->descricaoProcedimento, 'url' => ['view', 'id' => $model->idProcedimentos]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="procedimentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
