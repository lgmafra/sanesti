<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProcedimentosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="procedimentos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idProcedimentos') ?>

    <?= $form->field($model, 'procedimento') ?>

    <?= $form->field($model, 'profat') ?>

    <?= $form->field($model, 'descricaoProcedimento') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
