<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Prestador */

$this->title = 'Novo Prestador';
$this->params['breadcrumbs'][] = ['label' => 'Prestadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestador-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
