<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PrestadorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prestador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idPrestador') ?>

    <?= $form->field($model, 'codMVPrestador') ?>

    <?= $form->field($model, 'nomePrestador') ?>

    <?= $form->field($model, 'crmPrestador') ?>

    <?= $form->field($model, 'emailPrestador') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
