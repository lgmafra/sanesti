<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prestador */

$this->title = 'Atualizar Prestador: ' . $model->nomePrestador;
$this->params['breadcrumbs'][] = ['label' => 'Prestadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomePrestador, 'url' => ['view', 'id' => $model->idPrestador]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="prestador-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
