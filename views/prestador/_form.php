<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Prestador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="prestador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codMVPrestador')->textInput() ?>

    <?= $form->field($model, 'nomePrestador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'crmPrestador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emailPrestador')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
