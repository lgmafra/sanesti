<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PrestadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prestadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prestador-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Novo Prestador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idPrestador',
            'codMVPrestador',
            'nomePrestador',
            'crmPrestador',
            'emailPrestador:email',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
