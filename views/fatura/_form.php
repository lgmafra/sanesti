<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fatura */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fatura-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'valorFatura')->textInput() ?>

    <?= $form->field($model, 'valorGlosa')->textInput() ?>

    <?= $form->field($model, 'valorPago')->textInput() ?>

    <?= $form->field($model, 'boletim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tipoPagamento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idImportacaoFatura')->textInput() ?>

    <?= $form->field($model, 'idCirurgias')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
