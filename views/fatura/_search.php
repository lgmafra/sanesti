<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FaturaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fatura-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'idFatura') ?>

    <?= $form->field($model, 'valorFatura') ?>

    <?= $form->field($model, 'valorGlosa') ?>

    <?= $form->field($model, 'valorPago') ?>

    <?= $form->field($model, 'boletim') ?>

    <?php // echo $form->field($model, 'tipoPagamento') ?>

    <?php // echo $form->field($model, 'idImportacaoFatura') ?>

    <?php // echo $form->field($model, 'idCirurgias') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
