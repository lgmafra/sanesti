<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FaturaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Fatura';
$this->params['breadcrumbs'][] = ['label' => 'Importar Faturamento', 'url' => ['importacao-fatura/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fatura-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo Html::a('Create Fatura', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idFatura',
            //'boletim',
            [
                'label'=>'Atendimento',
                'value'=>function($data){
                    if(isset($data->cirurgiaFaturas[0]->cirurgias))
                        return $data->cirurgiaFaturas[0]->cirurgias->atendimentoCirurgia;
                }
            ],
            [
                'label'=>'Paciente',
                'value'=>function($data){
                    if(isset($data->cirurgiaFaturas[0]->cirurgias))
                        return $data->cirurgiaFaturas[0]->cirurgias->nomePaciente;
                }
            ],
            'valorFatura',
            'valorGlosa',
            //'valorPago',
            // 'tipoPagamento',
            // 'idImportacaoFatura',
            // 'idCirurgias',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
