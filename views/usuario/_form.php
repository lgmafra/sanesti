<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Prestador;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

$prestadores = ArrayHelper::map(
            Prestador::find()->orderBy('nomePrestador')->all(),
            'idPrestador',
            'nomePrestador'
        );
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomeUsuario')->textInput(['maxlength' => true]) ?>

    <div class="row">
        <div class="col-md-3">
    <?= $form->field($model, 'loginUsuario')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'emailUsuario')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
    <?php //if(!$model->isNewRecord): ?>
    <?= $form->field($model, 'senhaUsuario')->passwordInput(['maxlength' => true]) ?>
    <?php //endif; ?>
        </div>
    </div>

    <?php if(Yii::$app->user->identity->getIsUserAdmin()): ?>
    <div class="row">
        <div class="col-md-2">
    <?= $form->field($model, 'statusUsuario')->dropDownList([true=>'Sim', false=>'Não']) ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'idPrestador')->dropDownList($prestadores, ['prompt'=>'Nenhum']) ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'idNivel')->dropDownList(Usuario::NIVEL_USER, ['prompt'=>'Escolha']) ?>
        </div>
    </div>
    <?php endif; ?>
    
    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
