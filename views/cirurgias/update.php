<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cirurgias */

$this->title = 'Atualizar Cirurgia Atendimento: ' . $model->atendimentoCirurgia;
$this->params['breadcrumbs'][] = ['label' => 'Cirurgias', 'url' => ['index', 'id'=>$model->idImportacaoProducao]];
$this->params['breadcrumbs'][] = ['label' => $model->atendimentoCirurgia, 'url' => ['view', 'id' => $model->idCirurgias]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="cirurgias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelBoletim' => $modelBoletim
    ]) ?>

</div>
