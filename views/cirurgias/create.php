<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cirurgias */

$this->title = 'Cadastrar Cirurgia';
$this->params['breadcrumbs'][] = ['label' => 'Cirurgias', 'url' => ['index', 'id' => Yii::$app->request->get('id')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cirurgias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelBoletim' => $modelBoletim
    ]) ?>

</div>
