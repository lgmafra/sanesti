<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\CirurgiasSearch;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CirurgiasSearch */
/* @var $model app\models\Cirurgias */
/* @var $dataProvider yii\data\ActiveDataProvider */


$script = <<< JS
$(document).ready(function() {
    $("#busca_avancada").hide();
    
    $("#btn_busca_avancada").click(function(){
       $("#busca_avancada").toggle();
    });
});
JS;

$this->registerJs($script, \yii\web\VIEW::POS_LOAD);

if(Yii::$app->request->get('id') != null){
    $this->title = 'Cirurgias Competência: '.$searchModel->importacaoProducao->competenciaProducao;
    $attrDataCirurgia = [
                'attribute'=>'dataCirurgia',
                'filterInputOptions'=>['class'=>'form-control data']
            ];
}else{
    Yii::$app->session->set("params_url", Yii::$app->request->queryParams);

    $this->title = 'Cirurgias';
    $attrDataCirurgia = [
                'attribute'=>'dataCirurgia',
                'options' => [
                    'format' => 'DD/MM/YYYY',
                ],
                'filterType'=>  GridView::FILTER_DATE_RANGE,
                'filterWidgetOptions' => ([       
                    'attribute' => 'dataCirurgia',
                    //'presetDropdown' => true,
                    'convertFormat'=>false,
                    'pluginOptions' => [
                      'separator' => ' - ',
                      'format' => 'DD/MM/YYYY',
                      'locale' => [
                            'format' => 'DD/MM/YYYY'
                        ],
                    ],
                    'pluginEvents' => [
                        "apply.daterangepicker" => "function() { apply_filter('dataCirurgia') }",
                    ],
                ])
                //'group'=>true,
                //'subGroupOf'=>1
            ];
}

$this->params['breadcrumbs'][] = ['label' => 'Importar Produção', 'url' => ['importacao-producao/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cirurgias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div id="busca_avancada">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>

    <p>
        <span class="btn btn-info" id="btn_busca_avancada">Busca Avançada</span>
        <?php if(Yii::$app->request->get('id') != null): ?>
        <?= Html::a('Cadastrar Cirurgia', ['create', 'id'=>$searchModel->importacaoProducao->idImportacaoProducao], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php $defaultExportConfig = ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nomePaciente',
            'atendimentoCirurgia',
            'empresaCirurgia',
            'dataCirurgia',
            'codigoConvenio',
            'nomeConvenio',
            'procedimento',
            'descricaoProcedimento',
            'nomeCirurgiao',
            'nomeAnestesista',
            [
                'attribute'=> 'Valor Fatura',
                'label'=>'Valor Fatura',
                'hAlign'=>'right',
                'value'=>function($data){
                    return number_format(CirurgiasSearch::retornaValorFatura($data), 2, ".", "");
                }
            ],
            [
                'attribute'=> 'Valor Glosa',
                'label'=>'Valor Glosa',
                'hAlign'=>'right',
                'value'=>function($data){
                    return number_format(CirurgiasSearch::retornaValorGlosa($data), 2, ".", "");
                },
                'pageSummary'=>true,
                'pageSummaryFunc'=>GridView::F_SUM
            ],
            [
                'header'=>'Total',
                'value'=>function($data){
                    return number_format((CirurgiasSearch::retornaValorFatura($data)-CirurgiasSearch::retornaValorGlosa($data)), 2, ".", "");
                }
            ]
        ],
        'showConfirmAlert'=>false,
        'target' => ExportMenu::TARGET_BLANK,

    ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'font-size:12px;'],
        //'pjax'=>true,
        'striped'=>false,
        'hover'=>true,
        'showPageSummary'=>true,
//        'autoXlFormat'=>true,
        //'export'=>false,
        'toolbar' => [
            '{toggleData}',
            [
                'content'=>$defaultExportConfig
            ]
        ],
        //'toggleDataContainer' => ['class' => 'btn btn-default-sm'],
        'panel'=>[
            'type'=>GridView::TYPE_SUCCESS,
//            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Create Country', ['create'], ['class' => 'btn btn-success']),
//            'after'=>Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset Grid', ['index'], ['class' => 'btn btn-info']),
        ],
        'resizableColumns'=>true,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idCirurgias',
            
            
            //'carteiraPaciente',
            //'codPaciente',
            [
                'attribute'=>'nomePaciente',
                'group'=>true,
                //'groupedRow'=>true,
                //'subGroupOf'=>2,
                'groupHeader'=>function($model, $key, $index, $widget){
                    return[
                        'mergeColumns'=>[[0, 14]], // columns to merge in summary
                        'content'=>[             // content to show in each summary cell
                            0=>$model->nomePaciente
                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            0=>['class'=>'kv-grouped-row'],
                        ],
                    ];
                },
                //'groupOddCssClass'=>'kv-grouped-row',  // configure odd group cell css class
                //'groupEvenCssClass'=>'kv-grouped-row', // configure even group cell css class
                'groupFooter'=>function ($model, $key, $index, $widget) { // Closure method
                    return [
                        'mergeColumns'=>[[0, 9]], // columns to merge in summary
                        'content'=>[             // content to show in each summary cell
                            0=>'Total do Atendimento',
                            10=>GridView::F_SUM,
                            11=>GridView::F_SUM,
                            12=>GridView::F_SUM,
                        ],
                        'contentFormats'=>[      // content reformatting for each summary cell
                            10=>['format'=>'number', 'decimals'=>2, 'decPoint'=>'.', 'thousandSep'=>" "],
                            11=>['format'=>'number', 'decimals'=>2, 'decPoint'=>'.', 'thousandSep'=>" "],
                            12=>['format'=>'number', 'decimals'=>2, 'decPoint'=>'.', 'thousandSep'=>" "],
                        ],
                        'contentOptions'=>[      // content html attributes for each summary cell
                            //1=>['style'=>'font-variant:small-caps'],
                            10=>['style'=>'text-align:right'],
                            11=>['style'=>'text-align:right'],
                            12=>['style'=>'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options'=>['class'=>'success','style'=>'font-weight:bold;']
                    ];
                }
            ],
            [
                'attribute'=>'atendimentoCirurgia',
                'group'=>true
            ],
            [
                'attribute'=>'empresaCirurgia',
                'group'=>true,
                'subGroupOf'=>1
                //'groupedRow'=>true,
            ],
            // 'cidCirurgia',
            $attrDataCirurgia,
            // 'inicioCirurgia',
            // 'fimCirurgia',
            // 'codigoCirurgia',
            //'descricaoCirurgia',
            //'tipoAnestesia',
            [
                'attribute'=>'codigoConvenio',
                'filter'=>false
                //'filter'=>Html::activeDropDownList($searchModel, "codigoConvenio", $convenios, ['multiple'=>true, 'class'=>'form-control'])
                //'group'=>true,
                //'subGroupOf'=>1
            ],
            [
                'attribute'=>'nomeConvenio',
                'filter'=>false
                //'group'=>true,
                //'subGroupOf'=>1
            ],
            [
                'attribute'=>'procedimento',
                'filter'=>false
            ],
            //'profat',
            [
                'attribute'=>'descricaoProcedimento',
                'filter'=>false
            ],
            // 'codigoCirurgiao',
            [
                'attribute'=>'nomeCirurgiao',
                'filter'=>false
            ],
            // 'crmCirurgiao',
            //'codigoAnestesista',
            [
                'attribute'=>'nomeAnestesista',
                'filter'=>false,
                'pageSummary'=>'Total'
            ],
            //'crmAnestesista',
            // 'idPrestador',
            // 'codMVPrestador',
            // 'idImportacaoProducao',
            // 'boletim',
            [
                'attribute'=> 'Valor Fatura',
                'label'=>'Valor Fatura',
                'hAlign'=>'right',
                'value'=>function($data){
                    return number_format(CirurgiasSearch::retornaValorFatura($data), 2, ".", "");
                },
                'pageSummary'=>true,
                'pageSummaryFunc'=>GridView::F_SUM
            ],
            [
                'attribute'=> 'Valor Glosa',
                'label'=>'Valor Glosa',
                'hAlign'=>'right',
                'value'=>function($data){
                    return number_format(CirurgiasSearch::retornaValorGlosa($data), 2, ".", "");
                },
                'pageSummary'=>true,
                'pageSummaryFunc'=>GridView::F_SUM
            ],
//            [
//                'attribute'=>'nomePaciente',
//                'label'=>'Total',
//                'value'=>function($data){
//                    return \app\models\CirurgiasSearch::retornaTotal($data);
//                }
//            ],
                        
            [
                'class'=>'kartik\grid\FormulaColumn',
                'header'=>'Total',
                'value'=>function ($model, $key, $index, $widget) { 
                    $p = compact('model', 'key', 'index');
                    return number_format(($widget->col(10, $p) - $widget->col(11, $p)), 2, ".", "");
                },
                'hAlign'=>'right',
                'pageSummary'=>true,
                'pageSummaryFunc'=>GridView::F_SUM
            ],
            [
                'attribute'=>'boletim_valido',
                'header'=>'Situação Boletim',
                'format'=>'raw',
                'value'=>function($data){
                    if($data->cirurgiaFatura != null){
                        if(!empty($data->cirurgiaFatura[0]->boletim)){
                            return $data->boletim_valido ? Html::tag("i", '', ['class'=>'fa fa-check-circle']) : Html::tag("i", '', ['class'=>'fa fa-times-circle']);
                        }else{
                            return "";
                        }
                    }else{
                        return "";
                    }
                }
            ],

            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons'=>[
                    'delete' => function($url, $model){
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            ['#'], 
                            [
                                'title' => 'Excluir Registro',
                                'data-toggle' => "modal",
                                'data-target' => '#deletaImportacao',
                                'data-action' => $url,
                                'data-id' => $model->idCirurgias
                            ]
                        );
                    }
                ]
            ],
        ],
    ]); ?>
</div>


<div class="modal fade" id="deletaImportacao" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="modal-title" id="modalLabel"><strong>Alerta</strong></h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
        <div class="modal-body">
            Deseja realmente excluir esse registro?
        </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Não</button>
        <a class="btn btn-primary" id="urlDestino" href="#" >Sim</a>
      </div>
    </div>
  </div>
</div>