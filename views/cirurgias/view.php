<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\Usuario;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use app\models\Cirurgias;
use yii\helpers\ArrayHelper;
use app\models\Fatura;

/* @var $this yii\web\View */
/* @var $model app\models\Cirurgias */
/* @var $form yii\widgets\ActiveForm */

$arr_tipos_pagamentos = [
    Fatura::TIPO_PGTO_DINHEIRO => "Dinheiro",
    Fatura::TIPO_PGTO_CARTAO => "Cartão",
    Fatura::TIPO_PGTO_CONVENIO => "Convênio"
];

$this->title = "Atendimento: ".$model->atendimentoCirurgia;
$this->params['breadcrumbs'][] = ['label' => 'Cirurgias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cirurgias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cadastrar Novo', ['create', 'id'=>$model->idImportacaoProducao], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Atualizar', ['update', 'id' => $model->idCirurgias], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['delete', 'id' => $model->idCirurgias], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-secondary']) ?>
        
        <?php 
        if(in_array($model->codigoConvenio, Usuario::CONVENIOS_PARTICULAR)){
            
            Modal::begin([
                //'header' => "<h2>{$model->atendimentoCirurgia}</h2>",
                'toggleButton' => ['label' => '&nbsp;Valor do Procedimento', 'class'=>'btn btn-info'],
                'size'=>  Modal::SIZE_DEFAULT
            ]);
                Pjax::begin();
                    $form = ActiveForm::begin([
                        'options' => [
                            'data-pjax' => true
                        ]
                    ]);
                        $items = ArrayHelper::map(
                                Cirurgias::find()->where(['atendimentoCirurgia'=>$model->atendimentoCirurgia])->all(),
                                'profat', 
                                'descricaoProcedimento'
                                );
                        // < Dados da Fatura >
                        echo $form->field($modelCirFatura, 'procedimento')->dropDownList($items, ['prompt'=>'Escolha']);
                        
                        echo $form->field($modelImpFat, 'competenciaFatura')->textInput(['class'=>'form-control competencia']);

                        echo $form->field($modelFat, 'valorPago')->textInput(['class'=>'form-control']);

                        echo $form->field($modelFat, 'tipoPagamento')->dropDownList($arr_tipos_pagamentos, ['prompt'=>'Escolha']);

                        echo $form->field($modelFat, 'dataPagamento')->textInput(['class'=>'form-control data']);
                        
                        echo '<div class="form-group">';
                            echo Html::submitButton(Yii::t('app', 'Salvar'), ['class' => 'btn btn-primary']);//
                        echo '</div>';
                        //valorFatura
                        //echo Html::hiddenInput('idImportacaoFatura', 0); //idImportacaoFatura

                        // < Dados Cirurgia Fatura >
                        echo Html::hiddenInput('dataCirurgia', $model->dataCirurgia); //dataCirurgia
                        //echo Html::hiddenInput('procedimento', $model->procedimento); //procedimento
                        echo Html::hiddenInput('empresaFatura', 'S'); //empresaFatura
                        echo Html::hiddenInput('idCirurgias', $model->idCirurgias); //idCirurgia
                        //idFatura
                    $form->end();
                Pjax::end();
            Modal::end();
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'empresaCirurgia',
            'atendimentoCirurgia',
            'carteiraPaciente',
            'codPaciente',
            'nomePaciente',
            'cidCirurgia',
            'dataCirurgia',
            'inicioCirurgia',
            'fimCirurgia',
            [
                'attribute'=>'descricaoCirurgia',
                'value'=>function($data){
                    return $data->codigoCirurgia.' - '.$data->descricaoCirurgia;
                }
            ],
            //'codigoCirurgia',
            //'descricaoCirurgia',
            'tipoAnestesia',
            'procedimento',
            'profat',
            'descricaoProcedimento',
            [
                'attribute'=>'nomeConvenio',
                'value'=>function($data){
                    return $data->codigoConvenio.' - '.$data->nomeConvenio.' - '.$data->planoConvenio;
                }
            ],
            [
                'attribute'=>'acomodacao',
                'value'=>function($data){
                    return $data->codAcomodacao . ' - ' . $data->acomodacao;
                }
            ],
            //'codAcomodacao',
            //'acomodacao',
            'codigoCirurgiao',
            'nomeCirurgiao',
            'crmCirurgiao',
            'codigoAnestesista',
            'nomeAnestesista',
            'crmAnestesista',
            [
                'attribute' => 'boletim',
                'value' => function($data){
                    $boletins = [];
                    foreach ($data->cirurgiaFatura as $boletim){
                        if(!empty($boletim->boletim)){
                            $boletins[] = $boletim->boletim;
                        }
                    }
                    return count($boletins) > 0 ? implode(";", $boletins) : "";
                }
            ],
        ],
    ]) ?>

    <hr>
    <h3>Valores de Faturamento</h3>
    <?= GridView::widget([
        'dataProvider' => $modelFatura,
        'showFooter' => true,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idFatura',
            [
                'label'=>'Procedimento',
                'value'=>function($d){
                    return $d->cirurgiaFaturas[0]->procedimento;
                },
                'footer'=>'<b>Total Recebido</b>'
            ],
            [
                'attribute'=>'valorFatura',
                'footer'=>'<b>'.Fatura::getTotal($modelFatura->getModels(), 'valorFatura').'</b>'
            ],
            [
                'attribute'=>'valorGlosa',
                'footer'=>'<b>'.Fatura::getTotal($modelFatura->getModels(), 'valorGlosa').'</b>'
            ],
            [
                'attribute'=>'dataPagamento',
                'format' => ['date', 'php:d/m/Y'],
                'footer'=>'<b>'.(Fatura::getTotal($modelFatura->getModels(), 'valorFatura')-Fatura::getTotal($modelFatura->getModels(), 'valorGlosa')).'</b>'
            ],
            [
                'attribute'=>'tipoPagamento',
                'value'=>function($data){
                    $tipoPagamento = "";
                    switch ($data->tipoPagamento){
                        case Fatura::TIPO_PGTO_CONVENIO:
                            $tipoPagamento = "Convênio";
                            break;
                        case Fatura::TIPO_PGTO_DINHEIRO:
                            $tipoPagamento = "Dinheiro";
                            break;
                        case Fatura::TIPO_PGTO_CARTAO:
                            $tipoPagamento = "Cartão";
                            break;
                    }

                    return $tipoPagamento;
                }
            ],
            // 'valorPago',
            // 'tipoPagamento',
            // 'idImportacaoFatura',
            // 'idCirurgias',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'buttons' => [
                    'delete' => function($url, $model){
                        return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>', 
                                ['fatura/delete', 'id'=>$model->idFatura],
                                [
                                    'title' => 'Excluir Registro',
                                    'data' => [
                                        'confirm' => 'Tem certeza que deseja excluir este lançamento?',
                                        'method' => 'post',
                                    ],
                                ]
                        );
                    }
                ]
            ],
        ],
    ]); ?>
</div>
