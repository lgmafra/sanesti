<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Convenio;
use app\models\Procedimentos;
use app\models\Prestador;
use app\models\PrestadorMv;

/* @var $this yii\web\View */
/* @var $model app\models\CirurgiasSearch */
/* @var $form yii\widgets\ActiveForm */

$prestadores = ArrayHelper::map(
    Prestador::find()->orderBy('nomePrestador')->all(),
    'idPrestador',
    'nomePrestador'
);

$procedimentos = ArrayHelper::map(
    Procedimentos::find()->select('procedimento, descricaoProcedimento')->orderBy("descricaoProcedimento")->all(),
    'procedimento',
    function($field){
        return $field['procedimento'].' - '.$field['descricaoProcedimento'];
    }
);

$prestadoreMV = ArrayHelper::map(
    PrestadorMv::find()->all(),
    'codigoPrestador',
    function($field){
        return $field['codigoPrestador'].' - '.$field['nomePrestador'];
    }
);

$convenios = ArrayHelper::map(
    Convenio::find()->all(),
    'codigoConvenio',
    function($field){
        return $field['codigoConvenio'].' - '.$field['nomeConvenio'];
    }
);

?>

<div class="cirurgias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-2">
    <?= $form->field($model, 'empresaCirurgia')->dropDownList(['HCMF'=>'HCMF', 'HMN'=>'HMN'], ['class'=>'form-control', 'prompt'=>'-----------']) ?>
        </div>
        <div class="col-md-2">
    <?= $form->field($model, 'atendimentoCirurgia') ?>
        </div>
        <div class="col-md-6">
    <?php echo $form->field($model, 'nomePaciente') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'possuiPagamento')->dropDownList(['S'=>'Sim', 'N'=>'Não'], ['class'=>'form-control', 'prompt'=>'-----------']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'codigoCirurgiao')->widget(Select2::classname(), [
                'data' => $prestadoreMV,
                'language' => 'pt',
                'options' => [
                    'placeholder' => 'Escolha o cirurgião...',
                    'multiple'=>true
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <?php //echo $form->field($model, 'idPrestador')->dropDownList($prestadores, ['prompt'=>'Escolha']) ?>
            <?= $form->field($model, 'idPrestador')->widget(Select2::classname(), [
                'data' => $prestadores,
                'language' => 'pt',
                'options' => [
                    'placeholder' => 'Escolha o anestesista...',
                    'multiple'=>true
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
    <?= $form->field($model, 'codigoConvenio')->widget(Select2::classname(), [
        'data' => $convenios,
        'language' => 'pt',
        'options' => [
            'placeholder' => 'Escolha o convênio...',
            'multiple'=>true,
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
        </div>
        <div class="col-md-6">
    <?= $form->field($model, 'procedimento')->widget(Select2::classname(), [
        'data' => $procedimentos,
        'language' => 'pt',
        'options' => [
            'placeholder' => 'Escolha o procedimento...',
            'multiple' => true
        ],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-search">&nbsp;</i>Pesquisar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
