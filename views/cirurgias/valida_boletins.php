<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\ImportacaoProducao */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Validar Boletins';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="importacao-producao-form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'options'=>[
            'enctype' => 'multipart/form-data',
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->field($model, '_arquivoImportacao')->widget(FileInput::classname(), [
                'pluginOptions'=>[
                    'showPreview' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'showCancel' => false,
                    'browseClass' => 'btn btn-success',
                    'removeClass' => 'btn btn-danger',
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                ],
            ]);?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Validar Arquivo', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
