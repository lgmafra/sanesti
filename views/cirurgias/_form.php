<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Prestador;
use app\models\Cirurgias;
use app\models\Convenio;
use app\models\Pacientes;
use app\models\Procedimentos;
use app\models\PrestadorMv;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Cirurgias */
/* @var $form yii\widgets\ActiveForm */

$prestadores = ArrayHelper::map(
            Prestador::find()->orderBy('nomePrestador')->all(),
            'idPrestador',
            'nomePrestador'
        );

$cirurgias = ArrayHelper::map(
            Cirurgias::find()->select('codigoCirurgia, descricaoCirurgia')->distinct()->all(),
            'codigoCirurgia',
            function($field){
                return $field['codigoCirurgia'].' - '.$field['descricaoCirurgia'];
            }
        );
$cirurgias = [999999=>"999999 - UNICOR", 888888=>"888888 - Consulta"]+$cirurgias;

$pacientes = ArrayHelper::map(
            Pacientes::find()->select('codPaciente, nomePaciente')->orderBy("nomePaciente")->all(),
            'codPaciente',
            function($field){
                return $field['codPaciente'].' - '.$field['nomePaciente'];
            }
        );

$procedimentos = ArrayHelper::map(
            Procedimentos::find()->select('procedimento, descricaoProcedimento')->orderBy("descricaoProcedimento")->all(),
            'procedimento',
            function($field){
                return $field['procedimento'].' - '.$field['descricaoProcedimento'];
            }
        );
        
$anestesias = ArrayHelper::map(
            Cirurgias::find()->select('tipoAnestesia')->distinct()->all(),
            'tipoAnestesia',
            'tipoAnestesia'
        );
        
$convenios = ArrayHelper::map(
            Convenio::find()->all(),
            'codigoConvenio',
            function($field){
                return $field['codigoConvenio'].' - '.$field['nomeConvenio'];
            }
        );
        
$prestadoreMV = ArrayHelper::map(
            PrestadorMv::find()->all(),
            'codigoPrestador',
            function($field){
                return $field['codigoPrestador'].' - '.$field['nomePrestador'];
            }
        );
        
$boletins = [];
if(!$model->isNewRecord){
    foreach ($modelBoletim as $m){
        $boletins[] = $m->boletim;
    }
}

$boletins = implode(";", $boletins);

$readonly = ($model->codigoCirurgia != 999999 && !$model->isNewRecord);
?>

<div class="cirurgias-form">

    <?php $form = ActiveForm::begin([
//        'layout' => 'horizontal',
//        'fieldConfig' => [
//            'horizontalCssClasses' => [
//                'label' => 'col-sm-2',
//                //'offset' => 'col-sm-offset-2',
//                'wrapper' => 'col-sm-12',
//            ],
//        ],
    ]); ?>
    <div class="row">
        <div class="col-md-3">
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
    <?= $form->field($model, 'empresaCirurgia')->dropDownList(['HCMF'=>'HCMF', 'HMN'=>'HMN'], ['class'=>'form-control', 'prompt'=>'-----------']) ?>
        <?php else: ?>
    <?= $form->field($model, 'empresaCirurgia')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        <?php endif; ?>
        </div>
        <div class="col-md-3">
        <?= $form->field($model, 'dataCirurgia')->textInput(['class'=>'form-control data', 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-3">
    <?= $form->field($model, 'inicioCirurgia')->textInput(['class'=>'form-control hora', 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-3">
    <?= $form->field($model, 'fimCirurgia')->textInput(['class'=>'form-control hora', 'readonly'=>$readonly]) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-2">
    <?= $form->field($model, 'atendimentoCirurgia')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-3">
    <?= $form->field($model, 'carteiraPaciente')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
        <div class="col-md-7">
    <?= $form->field($model, 'codPaciente')->widget(Select2::classname(), [
            'data' => $pacientes,
            'language' => 'pt',
            'options' => ['placeholder' => 'Escolha o paciente...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>        
        </div>
        <?php else: ?>
        <div class="col-md-3">
    <?= $form->field($model, 'codPaciente')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-4">
    <?= $form->field($model, 'nomePaciente')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <?php endif ?>
    </div>
    
    <div class="row">
        <div class="col-md-1">
    <?= $form->field($model, 'cidCirurgia')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>  
        </div>
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
        <div class="col-md-6">
    <?= $form->field($model, 'codigoCirurgia')->widget(Select2::classname(), [
            'data' => $cirurgias,
            'language' => 'pt',
            'options' => ['placeholder' => 'Escolha a cirurgia...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
        </div>
        <?php else: ?>
        <div class="col-md-1">
    <?= $form->field($model, 'codigoCirurgia')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'descricaoCirurgia')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <?php endif; ?>
        <div class="col-md-2">
    <?= $form->field($model, 'codAcomodacao')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-3">
    <?= $form->field($model, 'acomodacao')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
    </div>

    <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
    
    <?= $form->field($model, 'tipoAnestesia')->widget(Select2::classname(), [
            'data' => $anestesias,
            'language' => 'pt',
            'options' => ['placeholder' => 'Escolha a anestesia...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
    
    <?php else: ?>
    
    <?= $form->field($model, 'tipoAnestesia')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
    
    <?php endif; ?>
    
    <div class="row">
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
        <div class="col-md-8">
    <?= $form->field($model, 'procedimento')->widget(Select2::classname(), [
            'data' => $procedimentos,
            'language' => 'pt',
            'options' => [
                'placeholder' => 'Escolha o procedimento...',
                'multiple' => true
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
        </div>
        <?php else: ?>
        <div class="col-md-2">
    <?= $form->field($model, 'procedimento')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>        
        <div class="col-md-2">
    <?= $form->field($model, 'profat')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-8">
    <?= $form->field($model, 'descricaoProcedimento')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <?php endif; ?>
    </div>
    
    <div class="row">
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
        <div class="col-md-8">
    <?= $form->field($model, 'codigoConvenio')->widget(Select2::classname(), [
            'data' => $convenios,
            'language' => 'pt',
            'options' => [
                'placeholder' => 'Escolha o convênio...'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
        </div>
        <?php else: ?>
        <div class="col-md-2">
    <?= $form->field($model, 'codigoConvenio')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'nomeConvenio')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-5">
    <?= $form->field($model, 'planoConvenio')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <?php endif; ?>
    </div>
    
    <div class="row">
        <?php if($model->isNewRecord || $model->codigoCirurgia == 999999): ?>
        <div class="col-md-8">
    <?= $form->field($model, 'codigoCirurgiao')->widget(Select2::classname(), [
            'data' => $prestadoreMV,
            'language' => 'pt',
            'options' => ['placeholder' => 'Escolha o cirurgião...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>
        </div>
        <?php else: ?>
        <div class="col-md-2">
    <?= $form->field($model, 'codigoCirurgiao')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-8">
    <?= $form->field($model, 'nomeCirurgiao')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-2">
    <?= $form->field($model, 'crmCirurgiao')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <?php endif; ?>
    </div>
    
    <?php if($model->codigoAnestesista == 0 || $model->isNewRecord || $model->codigoCirurgia == 999999): ?>
    
    <?php echo $form->field($model, 'idPrestador')->dropDownList($prestadores, ['prompt'=>'Escolha']) ?>
    
    <?php else: ?>
    
    <div class="row">
        <div class="col-md-2">
    <?= $form->field($model, 'codigoAnestesista')->textInput(['readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-8">
    <?= $form->field($model, 'nomeAnestesista')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
        <div class="col-md-2">
    <?= $form->field($model, 'crmAnestesista')->textInput(['maxlength' => true, 'readonly'=>$readonly]) ?>
        </div>
    </div>
    
    <?php endif; ?>

    <div class="row">
        <div class="col-md-4">
    <?=  $form->field($model, 'boletim', [
        'template' => '{label} <small><b>(Separar boletins com ; ponto-e-vírgula)</b></small>{input}'
    ])->textInput(['maxlength' => true, 'value' => $boletins]) ?>
        </div>
    </div>
    
    <?= $form->field($model, '_aplicarBoletim')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a('Voltar', ['index'], ['class' => 'btn btn-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
