<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fatura`.
 */
class m171123_235711_create_fatura_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('fatura', [
            'idFatura' => $this->primaryKey(),
            'valorFatura' => $this->decimal(10, 2)->notNull()->comment('Valor Fatura'),
            'valorGlosa' => $this->decimal(10, 2)->null()->comment('Valor Glosa'),
            'valorPago' => $this->decimal(10, 2)->notNull()->comment('Valor Pago'),
            'tipoPagamento' => $this->string(100)->null()->comment('Tipo Pagamento'),
            'idImportacaoFatura' => $this->integer()->notNull()->comment('Importação Fatura')
        ]);
        
        $this->createIndex('fatura_FKIndex_importacaoFatura', 'fatura', 'idImportacaoFatura');
        
        $this->addForeignKey('FK_importacaoFatura_fatura', 'fatura', 'idImportacaoFatura', 'importacaoFatura', 'idImportacaoFatura', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('fatura');
    }
}
