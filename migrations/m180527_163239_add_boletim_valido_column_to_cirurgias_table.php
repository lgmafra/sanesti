<?php

use yii\db\Migration;

/**
 * Handles adding boletim_valido to table `cirurgias`.
 */
class m180527_163239_add_boletim_valido_column_to_cirurgias_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("cirurgias", "boletim_valido", $this->boolean()->defaultValue(false)->comment("Boletim Validado"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("cirurgias", "boletim_valido");
    }
}
