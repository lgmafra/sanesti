<?php

use yii\db\Migration;

/**
 * Handles adding idUsuario to table `cirurgias`.
 */
class m171212_235410_add_idUsuario_column_to_cirurgias_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('cirurgias', 'idUsuario', $this->integer()->null()->comment('Usuário Alterou'));
        
        $this->createIndex('cirurgias_FKindex_usuario', 'cirurgias', 'idUsuario');
        
        $this->addForeignKey('FK_cirurgias_usuario', 'cirurgias', ['idUsuario'], 'usuario', ['idUsuario']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('cirurgias', 'idUsuario');
    }
}
