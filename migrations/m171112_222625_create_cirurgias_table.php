<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cirurgias`.
 */
class m171112_222625_create_cirurgias_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cirurgias', [
            'idCirurgias' => $this->primaryKey(),
            'empresaCirurgia' => $this->string(10)->notNull()->comment('Empresa'),
            'atendimentoCirurgia' => $this->integer()->notNull()->comment('Atendimento'),
            'carteiraPaciente' => $this->string(20)->null()->comment('Carteira'),
            'codPaciente' => $this->integer()->notNull()->comment('Código Paciente'),
            'nomePaciente' => $this->string(100)->notNull()->comment('Nome Paciente'),
            'cidCirurgia' => $this->string(20)->notNull()->comment('CID'),
            'dataCirurgia' => $this->date()->notNull()->comment('Data Cirurgia'),
            'inicioCirurgia' => $this->time()->notNull()->comment('Hora Inicio'),
            'fimCirurgia' => $this->time()->notNull()->comment('Hora Termino'),
            'codigoCirurgia' => $this->integer()->notNull()->comment('Código Cirurgia'),
            'descricaoCirurgia' => $this->string(255)->notNull()->comment('Descrição Cirurgia'),
            'tipoAnestesia' => $this->string(100)->notNull()->comment('Tipo Anestesia'),
            'procedimento' => $this->string(20)->notNull()->comment('Procedimento'),
            'profat' => $this->string(20)->null()->comment('profat'),
            'descricaoProcedimento' => $this->string(255)->null()->comment('Descricao Procedimento'),
            'codigoConvenio' => $this->integer()->notNull()->comment('Código Convênio'),
            'nomeConvenio' => $this->string(70)->notNull()->comment('Nome Convênio'),
            'planoConvenio' => $this->string(70)->null()->comment('Plano Convênio'),
            'codAcomodacao' => $this->integer()->null()->comment('Código Acomodação'),
            'acomodacao' => $this->string(100)->null()->comment('Acomodação'),
            'codigoCirurgiao' => $this->integer()->notNull()->comment('Código Cirurgião'),
            'nomeCirurgiao' => $this->string(70)->notNull()->comment('Nome Cirurgião'),
            'crmCirurgiao' => $this->string(20)->null()->comment('CRM Cirurgião'),
            'codigoAnestesista' => $this->integer()->notNull()->comment('Código Anestesista'),
            'nomeAnestesista' => $this->string(70)->notNull()->comment('Nome Anestesista'),
            'crmAnestesista' => $this->string(20)->null()->comment('CRM Anestesista'),
            'idPrestador' => $this->integer()->null()->comment('Prestador'),
            'codMVPrestador' => $this->integer()->null()->comment('Código MV Prestador'),
            'idImportacaoProducao' => $this->integer()->notNull()
        ]);
        
        $this->createIndex('cirurgias_FKIndex_prestador', 'cirurgias', 'idPrestador');
        $this->addForeignKey('FK_cirurgias_prestador', 'cirurgias', ['idPrestador'], 'prestador', ['idPrestador']);
        
        $this->createIndex('cirurgias_FKIndex_importacaoProducao', 'cirurgias', 'idImportacaoProducao');
        $this->addForeignKey(
            'FK_cirurgias_importacaoProducao', 
            'cirurgias', 
            ['idImportacaoProducao'], 
            'importacaoProducao', 
            ['idImportacaoProducao'],
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cirurgias');
    }
}
