<?php

use yii\db\Migration;

/**
 * Handles the creation of table `procedimentos`.
 */
class m180419_021041_create_procedimentos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('procedimentos', [
            'idProcedimentos' => $this->primaryKey(),
            'procedimento' => $this->string(20)->notNull()->comment('Procedimento'),
            'profat' => $this->string(20)->null()->comment('TUSS'),
            'descricaoProcedimento' => $this->string(255)->null()->comment('Descricao Procedimento'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('procedimentos');
    }
}
