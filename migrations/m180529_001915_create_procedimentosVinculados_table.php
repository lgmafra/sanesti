<?php

use yii\db\Migration;

/**
 * Handles the creation of table `procedimentosVinculados`.
 */
class m180529_001915_create_procedimentosVinculados_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('procedimentosVinculados', [
            'idProcedimentosVinculados' => $this->primaryKey(),
            'procedimento' => $this->string(15)->comment('Procedimento'),
            'procedimento_vinculado' => $this->string(15)->comment('Procedimento Vinculado'),
            'desc_proc_vinculado' => $this->string(255)->comment('Descrição Procedimento')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('procedimentosVinculados');
    }
}
