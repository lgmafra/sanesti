<?php

use yii\db\Migration;

/**
 * Handles adding codCirurgiaAviso to table `cirurgias`.
 */
class m180218_190956_add_codCirurgiaAviso_column_to_cirurgias_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn("cirurgias", "codCirurgiaAviso", $this->integer()->comment("Cirurgia Aviso"));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn("cirurgias", "codCirurgiaAviso");
    }
}
