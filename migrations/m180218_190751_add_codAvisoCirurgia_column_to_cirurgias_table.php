<?php

use yii\db\Migration;

/**
 * Handles adding codAvisoCirurgia to table `cirurgias`.
 */
class m180218_190751_add_codAvisoCirurgia_column_to_cirurgias_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn("cirurgias", "codAvisoCirurgia", $this->integer()->comment("Aviso Cirurgia"));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn("cirurgias", "codAvisoCirurgia");
    }
}
