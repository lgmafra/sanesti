<?php

use yii\db\Migration;

/**
 * Handles the creation of table `prestador`.
 */
class m171106_001444_create_prestador_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('prestador', [
            'idPrestador' => $this->primaryKey(),
            'codMVPrestador' => $this->integer()->unique()->notNull(),
            'nomePrestador' => $this->string(70)->notNull()->comment('Nome Prestador'),
            'crmPrestador' => $this->string(20)->notNull()->unique()->comment('CRM Prestador'),
            'emailPrestador' => $this->string(100)->notNull()->comment('Email Prestador')
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'7668',
            'nomePrestador'=>"ROSANIA RIBEIRO DA SILVA LIMA",
            'crmPrestador'=>"24140",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'7064',
            'nomePrestador'=>"RAFAEL CANO RIBEIRO",
            'crmPrestador'=>"24.636",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'33',
            'nomePrestador'=>"ANTONIO ANDRADE BRITO FILHO",
            'crmPrestador'=>"6768",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'42',
            'nomePrestador'=>"PAULO DE TARSO SANTOS PINHEIRO",
            'crmPrestador'=>"9218",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'38',
            'nomePrestador'=>"LUCIDIO HIPOLITO BRAGA LIBORIO",
            'crmPrestador'=>"4408",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'182',
            'nomePrestador'=>"ISAAC ROMEU MOREIRA RIBEIRO",
            'crmPrestador'=>"6354",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'43',
            'nomePrestador'=>"TELMA REGINA OLIVEIRA DANTAS",
            'crmPrestador'=>"6927",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'44',
            'nomePrestador'=>"ANGELA DE SANTA TEREZA DE ALMEIDA FARIAS",
            'crmPrestador'=>"6463",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
        
        $this->insert('prestador', [
            'codMVPrestador'=>'40',
            'nomePrestador'=>"NINA ROSA NUNES BRANDAO",
            'crmPrestador'=>"11180",
            'emailPrestador'=>'NAO INFORMADO'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('prestador');
    }
}
