<?php

use yii\db\Migration;

/**
 * Handles adding arquivo to table `cirurgias`.
 */
class m171115_183412_add_arquivo_column_to_importacaoproducao_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('importacaoProducao', 'arquivo', $this->string(30)->null()->comment('Arquivo Importação'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('importacaoProducao', 'arquivo');
    }
}
