<?php

use yii\db\Migration;

/**
 * Handles the creation of table `prestador_mv`.
 */
class m180403_231958_create_prestador_mv_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('prestador_mv', [
            'idPrestadorMV' => $this->primaryKey(),
            'codigoPrestador' => $this->integer()->notNull(),
            'nomePrestador' => $this->string(150)->notNull(),
            'crmPrestador' => $this->string(20)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prestador_mv');
    }
}
