<?php

use yii\db\Migration;

/**
 * Handles adding dataPagamento to table `fatura`.
 */
class m171203_021620_add_dataPagamento_column_to_fatura_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('fatura', 'dataPagamento', $this->date()->null()->comment('Data Pagamenot'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('fatura', 'dataPagamento');
    }
}
