<?php

use yii\db\Migration;

/**
 * Handles adding idNivel to table `usuario`.
 */
class m180224_163227_add_idNivel_column_to_usuario_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn("usuario", "idNivel", $this->integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn("usuario", "idNivel");
    }
}
