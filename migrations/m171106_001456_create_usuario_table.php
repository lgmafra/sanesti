<?php

use yii\db\Migration;

/**
 * Handles the creation of table `usuario`.
 */
class m171106_001456_create_usuario_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('usuario', [
            'idUsuario' => $this->primaryKey(),
            'nomeUsuario' => $this->string(70)->notNull()->comment('Nome Usuário'),
            'loginUsuario' => $this->string(60)->notNull()->unique()->comment('Login'),
            'emailUsuario' => $this->string(100)->notNull()->unique()->comment('Email'),
            'senhaUsuario' => $this->string(100)->notNull()->comment('Senha'),
            'statusUsuario' => $this->boolean()->notNull()->defaultValue(true)->comment('Status'),
            'idPrestador' => $this->integer()->null()->comment('Prestador')
        ]);
        
        $this->createIndex(
                'usuario_FKIndex_prestador', 
                'usuario', 
                ['idPrestador']
        );
        
        $this->addForeignKey(
                'FK_usuario_prestador', 
                'usuario', 
                ['idPrestador'], 
                'prestador', 
                ['idPrestador']
        );
        
        $this->insert('usuario', [
            'nomeUsuario' => 'Administrador',
            'loginUsuario' => 'admin',
            'emailUsuario' => 'lgmafra@gmail.com',
            'senhaUsuario' => sha1('admin')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('usuario');
    }
}
