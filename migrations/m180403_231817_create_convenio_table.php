<?php

use yii\db\Migration;

/**
 * Handles the creation of table `convenio`.
 */
class m180403_231817_create_convenio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('convenio', [
            'idConvenio' => $this->primaryKey(),
            'codigoConvenio' => $this->integer()->notNull(),
            'nomeConvenio' => $this->string(150)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('convenio');
    }
}
