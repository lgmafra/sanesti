<?php

use yii\db\Migration;

/**
 * Handles the creation of table `importacaoProducao`.
 */
class m171112_222600_create_importacaoProducao_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('importacaoProducao', [
            'idImportacaoProducao' => $this->primaryKey(),
            'competenciaProducao' => $this->date()->notNull()->comment('Competência Produção'),
            'dataInicial' => $this->date()->notNull()->comment('Data Inicial'),
            'dataFinal' => $this->date()->notNull()->comment('Data Final'),
            'descricao' => $this->string(100)->notNull()->comment('Descrição'),
            'idUsuario' => $this->integer()->notNull()->comment('Usuário Importou')
        ]);
        
        $this->createIndex('importacaoProducao_FKindex_usuario', 'importacaoProducao', 'idUsuario');
        
        $this->addForeignKey('FK_importacaoProducao_usuario', 'importacaoProducao', ['idUsuario'], 'usuario', ['idUsuario']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('importacaoProducao');
    }
}
