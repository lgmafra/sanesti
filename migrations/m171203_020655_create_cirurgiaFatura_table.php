<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cirurgiaFatura`.
 */
class m171203_020655_create_cirurgiaFatura_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cirurgiaFatura', [
            'idCirurgiaFatura' => $this->primaryKey(),
            'boletim' => $this->integer()->null()->comment('Boletim'),
            'dataCirurgia' => $this->date()->notNull()->comment('Data Cirurgia'),
            'procedimento' => $this->string(20)->notNull()->comment('Procedimento'),
            'empresaFatura' => $this->string(1)->notNull()->comment('Empresa Fatura'),
            'idCirurgias' => $this->integer()->null()->comment('Cirurgia'),
            'idFatura' => $this->integer()->null()->comment('Fatura')
        ]);
        
        $this->createIndex('cirurgiaFatura_FKIndex_fatura', 'cirurgiaFatura', 'idFatura');
        $this->createIndex('cirurgiaFatura_FKIndex_cirurgias', 'cirurgiaFatura', 'idCirurgias');
        
        $this->addForeignKey('FK_fatura_cirurgiaFatura', 'cirurgiaFatura', 'idFatura', 'fatura', 'idFatura', 'SET NULL');
        $this->addForeignKey('FK_cirurgias_cirurgiaFatura', 'cirurgiaFatura', 'idCirurgias', 'cirurgias', 'idCirurgias', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cirurgiaFatura');
    }
}
