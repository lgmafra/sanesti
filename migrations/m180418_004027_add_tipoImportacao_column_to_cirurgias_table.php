<?php

use yii\db\Migration;

/**
 * Handles adding tipoImportacao to table `cirurgias`.
 */
class m180418_004027_add_tipoImportacao_column_to_cirurgias_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("cirurgias", "tipoImportacao", $this->string(1)->defaultValue("P"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("cirurgias", "tipoImportacao");
    }
}
