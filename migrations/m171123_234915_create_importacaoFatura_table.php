<?php

use yii\db\Migration;

/**
 * Handles the creation of table `importacaoFatura`.
 */
class m171123_234915_create_importacaoFatura_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('importacaoFatura', [
            'idImportacaoFatura' => $this->primaryKey(),
            'competenciaFatura' => $this->date()->notNull()->comment('Competência Fatura'),
            'descricao' => $this->string(100)->notNull()->comment('Descrição'),
            'empresaFatura' => $this->string(1)->notNull()->comment('Empresa Fatura'),
            'idUsuario' => $this->integer()->notNull()->comment('Usuário')
        ]);
        
        $this->createIndex('importacaoFatura_FKIndex_usuario', 'importacaoFatura', 'idUsuario');
        $this->addForeignKey('FK_usuario_importacaoFatura', 'importacaoFatura', 'idUsuario', 'usuario', 'idUsuario');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('importacaoFatura');
    }
}
