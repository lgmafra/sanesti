<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pacientes`.
 */
class m180419_021059_create_pacientes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pacientes', [
            'idPacientes' => $this->primaryKey(),
            'codPaciente' => $this->integer()->notNull()->comment('Código Paciente MV'),
            'nomePaciente' => $this->string(100)->notNull()->comment('Nome Paciente')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pacientes');
    }
}
