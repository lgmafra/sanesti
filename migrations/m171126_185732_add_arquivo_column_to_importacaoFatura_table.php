<?php

use yii\db\Migration;

/**
 * Handles adding arquivo to table `importacaoFatura`.
 */
class m171126_185732_add_arquivo_column_to_importacaoFatura_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('importacaoFatura', 'arquivo', $this->string(30)->null()->comment('Arquivo Importação'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('importacaoFatura', 'arquivo');
    }
}
