<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pacotes`.
 */
class m180603_233559_create_pacotes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pacotes', [
            'idPacotes' => $this->primaryKey(),
            'procedimento' => $this->string(15)->comment('Procedimento'),
            'pacote' => $this->string(15)->comment('Pacote'),
            'desc_pacote' => $this->string(255)->comment('Descrição Pacote')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pacotes');
    }
}
