<?php

use yii\db\Migration;

/**
 * Handles adding tipoImportacao to table `importacaoProducao`.
 */
class m180418_012355_add_tipoImportacao_column_to_importacaoProducao_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn("importacaoProducao", "tipoImportacao", $this->string(1)->defaultValue("P"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn("importacaoProducao", "tipoImportacao");
    }
}
