<?php

namespace app\controllers;

use Yii;
use app\models\Cirurgias;
use app\models\CirurgiasSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use app\models\CirurgiaFatura;
use app\models\Fatura;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Prestador;
use app\models\ImportacaoFatura;
use yii\base\Exception;
use app\models\PrestadorMv;
use app\models\Pacientes;
use app\models\Procedimentos;
use yii\web\UploadedFile;

/**
 * CirurgiasController implements the CRUD actions for Cirurgias model.
 */
class CirurgiasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update', 'validaboletins'],
                'rules' => [
                    [
                        'actions' => ['create', 'delete', 'update', 'validaboletins'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback'=>function(){
                            return \Yii::$app->user->identity->getIsUserAdmin();
                        }
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all Cirurgias models.
     * @return mixed
     */
    public function actionIndex($id = 0)
    {
        $searchModel = new CirurgiasSearch();

        if(Yii::$app->request->get()){
            if($id > 0){
                $cirurgiasSearch = isset(Yii::$app->request->queryParams['CirurgiasSearch']) ? Yii::$app->request->queryParams['CirurgiasSearch'] : [];
                $params = array_merge($cirurgiasSearch, ['idImportacaoProducao'=>$id]);

                $dataProvider = $searchModel->search(['CirurgiasSearch'=>$params]);
            }else{
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }
        }else{
            if(Yii::$app->session->get("params_url") != null){
                Yii::$app->request->queryParams = Yii::$app->session->get("params_url");
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }else{
                $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cirurgias model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        $modelFat = new Fatura();
        $modelFat->setScenario("view");
        $modelImpFat = new ImportacaoFatura();
        $modelCirFatura = new CirurgiaFatura();
        
        if(Yii::$app->request->post()){
            $competenciaFatura = Yii::$app->request->post('ImportacaoFatura')['competenciaFatura'];
            $valorPago = Yii::$app->request->post('Fatura')['valorPago'];
            $tipoPagamento = Yii::$app->request->post('Fatura')['tipoPagamento'];
            $dataPagamento = Yii::$app->request->post('Fatura')['dataPagamento'];
            $procedimento = Yii::$app->request->post('CirurgiaFatura')['procedimento'];
            
            if($competenciaFatura != "" && $valorPago != ""){
                $modelImp = ImportacaoFatura::find()->where(['competenciaFatura'=>'01/'.$competenciaFatura, 'empresaFatura'=>"P"])->one();
                if($modelImp == null){
                    $modelImp = new ImportacaoFatura();
                    $modelImp->competenciaFatura = $competenciaFatura;
                    $modelImp->descricao = "Importação de Faturamento Competência: ".$competenciaFatura." - Particular";
                    $modelImp->empresaFatura = "P";
                    $modelImp->idUsuario = Yii::$app->user->getId();
                    $modelImp->arquivo = "Importação Particular";
                    $modelImp->save(false);
                }
                $modelFat->valorFatura = $valorPago;
                $modelFat->valorGlosa = 0;
                $modelFat->valorPago = $valorPago;
                $modelFat->tipoPagamento = $tipoPagamento;
                $modelFat->dataPagamento = $dataPagamento == "" ? Yii::$app->request->post('dataCirurgia') : $dataPagamento;
                $modelFat->idImportacaoFatura = $modelImp->idImportacaoFatura;

                $modelFat->save();

                $modelCirFatura->idFatura = $modelFat->idFatura;
                $modelCirFatura->idCirurgias = Yii::$app->request->post('idCirurgias');
                $modelCirFatura->procedimento = $procedimento;
                $modelCirFatura->empresaFatura = 'S';
                $modelCirFatura->dataCirurgia = Yii::$app->request->post('dataCirurgia');

                $modelCirFatura->save();

                return $this->redirect(['view', 'id'=>$id]);
            }else{
                $modelImpFat->addErrors(['competenciaFatura'=>'Informe a competência de faturamento.']);
                $modelFat->addErrors(['valorPago'=>'Informe o valor pago.']);
            }
        }
        
        $ids = Cirurgias::find()->select('idCirurgias')->where(['atendimentoCirurgia'=>$model->atendimentoCirurgia])->asArray()->all();
        
        $modelFatura = Fatura::find()->join('JOIN', 'cirurgiaFatura', '"cirurgiaFatura"."idFatura" = fatura."idFatura"')
                ->join('JOIN', 'cirurgias', '"cirurgiaFatura"."idCirurgias" = cirurgias."idCirurgias"')
                ->where(['in', 'cirurgias."idCirurgias"', ArrayHelper::getColumn($ids, 'idCirurgias')])->all();
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $modelFatura,
        ]);
        
        return $this->render('view', [
            'model' => $model,
            'modelFatura' => $dataProvider,
            'modelFat' => $modelFat,
            'modelCirFatura' => $modelCirFatura,
            'modelImpFat' => $modelImpFat
        ]);
    }

    /**
     * Creates a new Cirurgias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Cirurgias();
        $modelBoletim = new CirurgiaFatura();
        
        $model->idImportacaoProducao = $id;
        $model->idUsuario = \Yii::$app->user->getId();
        
        if ($model->load(Yii::$app->request->post())) {
            $procedimentos = $model->procedimento;

            foreach ($procedimentos as $procedimento){
                $model->isNewRecord = true;
                unset($model->idCirurgias);

                $model->nomeConvenio = $model->retornaValorCampo($model->codigoConvenio, "nomeConvenio", "codigoConvenio")->nomeConvenio;
                $model->codigoAnestesista = $model->retornaValorCampo($model->idPrestador, "codigoAnestesista", "idPrestador")->codigoAnestesista;
                $model->nomeAnestesista = $model->retornaValorCampo($model->idPrestador, "nomeAnestesista", "idPrestador")->nomeAnestesista;
                $model->crmAnestesista = $model->retornaValorCampo($model->idPrestador, "crmAnestesista", "idPrestador")->crmAnestesista;

                $model->nomePaciente = Pacientes::find()->select("nomePaciente")->where(["codPaciente"=>$model->codPaciente])->one()->nomePaciente;
                $model->procedimento = $procedimento;
                $model->descricaoProcedimento = Procedimentos::find()->select("descricaoProcedimento")->where(["procedimento"=>$procedimento])->one()->descricaoProcedimento;
                $model->nomeCirurgiao = PrestadorMv::find()->select("nomePrestador")->where(["codigoPrestador"=>$model->codigoCirurgiao])->one()->nomePrestador;
                $model->crmCirurgiao = PrestadorMv::find()->select("crmPrestador")->where(["codigoPrestador"=>$model->codigoCirurgiao])->one()->crmPrestador;

                if($model->codigoCirurgia == "999999" || $model->codigoCirurgia == 0){
                    $model->descricaoCirurgia = "UNICOR";
                }else{
                    $model->descricaoCirurgia = $model->retornaValorCampo($model->codigoCirurgia, "descricaoCirurgia", "codigoCirurgia")->descricaoCirurgia;
                }

                $model->save(false);
            }

            $boletins = explode(";", $model->boletim);
            
            foreach ($boletins as $b){
                if(!empty($b)){
                    $modelBoletim = new CirurgiaFatura();
                    
                    if($model->_aplicarBoletim){
                        $cirurgias = Cirurgias::findAll(['atendimentoCirurgia'=> $model->atendimentoCirurgia]);
                        foreach ($cirurgias as $c){
                            $modelBoletim->isNewRecord = true;
                            unset($modelBoletim->idCirurgiaFatura);
                            $modelBoletim->boletim = $b;
                            $modelBoletim->dataCirurgia = $c->dataCirurgia;
                            $modelBoletim->procedimento = $c->procedimento;
                            $modelBoletim->empresaFatura = 'C';
                            $modelBoletim->idCirurgias = $c->idCirurgias;
                            $modelBoletim->save();
                        }
                    }else{
                        $modelBoletim->isNewRecord = true;
                        $modelBoletim->boletim = $b;
                        $modelBoletim->dataCirurgia = $model->dataCirurgia;
                        $modelBoletim->procedimento = $model->procedimento;
                        $modelBoletim->empresaFatura = 'C';
                        $modelBoletim->idCirurgias = $model->idCirurgias;
                        $modelBoletim->save();
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->idCirurgias]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'modelBoletim'=>$modelBoletim
            ]);
        }
    }

    /**
     * Updates an existing Cirurgias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelBoletim = CirurgiaFatura::find()->where(['idCirurgias'=>$model->idCirurgias])->all();
        
        $model->idUsuario = \Yii::$app->user->getId();
        
        if ($model->load(Yii::$app->request->post())) {
            $procedimentos = $model->procedimento;

            if(is_array($procedimentos)){
                foreach ($procedimentos as $index=>$procedimento){
                    if($index > 0){
                        $model->isNewRecord = true;
                    }

                    unset($model->idCirurgias);

                    $model->nomeConvenio = $model->retornaValorCampo($model->codigoConvenio, "nomeConvenio", "codigoConvenio")->nomeConvenio;
                    $model->codigoAnestesista = $model->retornaValorCampo($model->idPrestador, "codigoAnestesista", "idPrestador")->codigoAnestesista;
                    $model->nomeAnestesista = $model->retornaValorCampo($model->idPrestador, "nomeAnestesista", "idPrestador")->nomeAnestesista;
                    $model->crmAnestesista = $model->retornaValorCampo($model->idPrestador, "crmAnestesista", "idPrestador")->crmAnestesista;

                    $model->nomePaciente = Pacientes::find()->select("nomePaciente")->where(["codPaciente"=>$model->codPaciente])->one()->nomePaciente;
                    $model->procedimento = $procedimento;
                    $model->descricaoProcedimento = Procedimentos::find()->select("descricaoProcedimento")->where(["procedimento"=>$procedimento])->one()->descricaoProcedimento;
                    $model->nomeCirurgiao = PrestadorMv::find()->select("nomePrestador")->where(["codigoPrestador"=>$model->codigoCirurgiao])->one()->nomePrestador;
                    $model->crmCirurgiao = PrestadorMv::find()->select("crmPrestador")->where(["codigoPrestador"=>$model->codigoCirurgiao])->one()->crmPrestador;

                    if($model->codigoCirurgia == "999999" || $model->codigoCirurgia == 0){
                        $model->descricaoCirurgia = "UNICOR";
                    }else{
                        $model->descricaoCirurgia = $model->retornaValorCampo($model->codigoCirurgia, "descricaoCirurgia", "codigoCirurgia")->descricaoCirurgia;
                    }

                    $model->save(false);
                }
            }
            
            $boletins = explode(";", $model->boletim);
            
            if($model->cirurgiaFatura != null){
                if($model->cirurgiaFatura[0]->empresaFatura == 'C'){
                    if($model->_aplicarBoletim){
                        foreach ($boletins as $boletim){
                            $cirFatura = CirurgiaFatura::findAll(['boletim'=>$boletim]);
                            foreach ($cirFatura as $fatura){
                                if($fatura->fatura != null){
                                    Fatura::deleteAll(["idFatura"=>$fatura->idFatura]);
                                }
                            }
                            CirurgiaFatura::deleteAll(['boletim'=>$boletim]);
                        }
                    }else{
                        Fatura::deleteAll(["idFatura"=>$model->cirurgiaFatura[0]->idFatura]);
                        CirurgiaFatura::deleteAll(['idCirurgias'=>$model->idCirurgias]);
                    }
                }
            }
            
            foreach ($boletins as $b){
                if(!empty($b)){
                    $modelBoletim = new CirurgiaFatura();
                    
                    if($model->_aplicarBoletim){
                        $cirurgias = Cirurgias::findAll(['atendimentoCirurgia'=> $model->atendimentoCirurgia]);
                        foreach ($cirurgias as $c){
                            $modelBoletim->isNewRecord = true;
                            unset($modelBoletim->idCirurgiaFatura);
                            $modelBoletim->boletim = $b;
                            $modelBoletim->dataCirurgia = $c->dataCirurgia;
                            $modelBoletim->procedimento = $c->procedimento;
                            $modelBoletim->empresaFatura = 'C';
                            $modelBoletim->idCirurgias = $c->idCirurgias != null ? $c->idCirurgias : $id;
                            $modelBoletim->save();
                        }
                    }else{
                        $modelBoletim->isNewRecord = true;
                        $modelBoletim->boletim = $b;
                        $modelBoletim->dataCirurgia = $model->dataCirurgia;
                        $modelBoletim->procedimento = $model->procedimento;
                        $modelBoletim->empresaFatura = 'C';
                        $modelBoletim->idCirurgias = $model->idCirurgias != null ? $model->idCirurgias : $id;
                        $modelBoletim->save();
                    }
                }
            }

            if($model->idCirurgias != null){
                return $this->redirect(['view', 'id' => $model->idCirurgias]);
            }else{
                return $this->redirect(['view', 'id' => $id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'modelBoletim' => $modelBoletim
            ]);
        }
    }

    /**
     * Deletes an existing Cirurgias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $cirurgia = $this->findModel($id);
        
        if(count($cirurgia->cirurgiaFatura) > 0){
            foreach ($cirurgia->cirurgiaFatura as $c){
                if(!is_null($c->idFatura)){
                    Yii::$app->session->setFlash("danger", "Existe pagamento vinculado a esse lançamento. Para excluir, remova primeiro o pagamento.");
                    return $this->redirect(['view', 'id'=>$id]);
                }
            }
        }
        
        //$idImportacao = $cirurgia->idImportacaoProducao;
        $cirurgia->delete();

        return $this->redirect(['index']);
    }

    public function actionValidaboletins(){
        $model = new Cirurgias();

        if($model->load(Yii::$app->request->post())){
            $uploadedFile = UploadedFile::getInstance($model, "_arquivoImportacao");
            if(is_file($uploadedFile->tempName)){
                $fileName = $model->getNameFile($uploadedFile->extension);
                $uploadedFile->saveAs($model->getPathFile().$fileName);

                $cirFaturaModel = new CirurgiaFatura();
                $cirFaturaModel->validaBoletins($model->getPathFile().$fileName);

                Yii::$app->session->setFlash('success', 'Validação realizada com sucesso');

                return $this->redirect(['validaboletins']);
            }
        }

        return $this->render('valida_boletins', [
            'model'=>$model
        ]);
    }

    /**
     * Finds the Cirurgias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cirurgias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cirurgias::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Cirurgia não localizada');
        }
    }
}
