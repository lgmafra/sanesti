<?php

namespace app\controllers;

use Yii;
use app\models\ImportacaoProducao;
use app\models\ImportacaoProducaoSearch;
use app\models\Cirurgias;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\base\Exception;
use DateTime;

/**
 * ImportacaoProducaoController implements the CRUD actions for ImportacaoProducao model.
 */
class ImportacaoProducaoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update'],
                'rules' => [
                    [
                        'actions' => ['create', 'delete', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback'=>function(){
                            return \Yii::$app->user->identity->getIsUserAdmin();
                        }
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions'=>['view'],
                        'allow'=>false
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all ImportacaoProducao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImportacaoProducaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ImportacaoProducao model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ImportacaoProducao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImportacaoProducao();

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            try{
                $model->validaCompetenciaImportada();
                
                $uploadedFile = UploadedFile::getInstance($model, "_arquivoImportacao");
                if(is_file($uploadedFile->tempName)){
                    $fileName = $model->getNameFile($uploadedFile->extension);
                    $uploadedFile->saveAs($model->getPathFile().$fileName);
                    
                    $model->arquivo = $fileName;
                    $model->idUsuario = Yii::$app->user->getId();
                    
                    $model->save(false);
                    
                    $cirurgiaModel = new Cirurgias();
                    if($model->tipoImportacao == "P") {
                        $cirurgiaModel->importaProducao($model->getPathFile().$fileName, $model);
                    }else if($model->tipoImportacao == "C"){
                        $cirurgiaModel->importaConsultas($model->getPathFile().$fileName, $model);
                    }
                    
                    $trans->commit();
                    
                    Yii::$app->session->setFlash('success', 'Importação realizada com sucesso');
                
                    return $this->redirect(['/cirurgias/', 'id'=>$model->idImportacaoProducao]);
                }
            } catch (Exception $ex) {
                $trans->rollBack();

                $model->competenciaProducao = DateTime::createFromFormat('d/m/Y', "01/".$model->competenciaProducao)->format('m/Y');
                
                Yii::$app->session->setFlash("danger", $ex->getMessage());
                
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImportacaoProducao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            try{
                $uploadedFile = UploadedFile::getInstance($model, "_arquivoImportacao");
                if(is_file($uploadedFile->tempName)){
                    $fileName = $model->getNameFile($uploadedFile->extension);
                    $uploadedFile->saveAs($model->getPathFile().$fileName);
                    
                    $model->arquivo = $fileName;
                    $model->idUsuario = Yii::$app->user->getId();
                    
                    $model->save(false);
                    
                    $cirurgiaModel = new Cirurgias();
                    if($model->tipoImportacao == "P") {
                        $cirurgiaModel->importaProducao($model->getPathFile().$fileName, $model);
                    }else if($model->tipoImportacao == "C"){
                        $cirurgiaModel->importaConsultas($model->getPathFile().$fileName, $model);
                    }
                    
                    $trans->commit();
                    
                    Yii::$app->session->setFlash('success', 'Importação realizada com sucesso');
                
                    return $this->redirect(['/cirurgias/', 'id'=>$model->idImportacaoProducao]);
                }
            } catch (Exception $ex) {
                $trans->rollBack();
                
                $model->competenciaProducao = DateTime::createFromFormat('d/m/Y', "01/".$model->competenciaProducao)->format('m/Y');
                
                Yii::$app->session->setFlash("danger", $ex->getMessage());
                
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ImportacaoProducao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $imp = $this->findModel($id);
        $competencia = $imp->competenciaProducao;
        
        if($imp->delete() > 0)
            Yii::$app->session->setFlash('info', "Importação da competência {$competencia} removida com sucesso.");

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImportacaoProducao model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImportacaoProducao the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImportacaoProducao::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
