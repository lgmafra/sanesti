<?php

namespace app\controllers;

use Yii;
use app\models\ImportacaoFatura;
use app\models\ImportacaoFaturaSearch;
use app\models\Fatura;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\base\Exception;
use DateTime;

/**
 * ImportacaoFaturaController implements the CRUD actions for ImportacaoFatura model.
 */
class ImportacaoFaturaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'delete', 'view', 'update', 'reprocessar'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'delete', 'reprocessar'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback'=>function(){
                            return \Yii::$app->user->identity->getIsUserAdmin();
                        }
                    ],
                    [
                        'actions'=>['view', 'update'],
                        'allow'=>false
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all ImportacaoFatura models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ImportacaoFaturaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ImportacaoFatura model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ImportacaoFatura model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImportacaoFatura();

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            
            try{
                $model->validaCompetenciaImportada();
                
                $uploadedFile = UploadedFile::getInstance($model, "_arquivoImportacao");
                if(is_file($uploadedFile->tempName)){
                    $fileName = $model->getNameFile($uploadedFile->extension);
                    $uploadedFile->saveAs($model->getPathFile().$fileName);
                    
                    $model->arquivo = $fileName;
                    $model->idUsuario = Yii::$app->user->getId();
                    
                    $model->save(false);
                    
                    $faturaModel = new Fatura();
                    $faturaModel->importaFaturamento($model->getPathFile().$fileName, $model, $model->dataPagamento);
                    
                    $trans->commit();
                    
                    Yii::$app->session->setFlash('success', 'Importação realizada com sucesso');
                
                    return $this->redirect(['index']);
                }
            } catch (Exception $ex) {
                $trans->rollBack();

                $model->competenciaFatura = DateTime::createFromFormat('d/m/Y', $model->competenciaFatura)->format('m/Y');
                
                Yii::$app->session->setFlash("danger", $ex->getMessage());
                
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ImportacaoFatura model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idImportacaoFatura]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ImportacaoFatura model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        if($model->empresaFatura != "C"){
            foreach ($model->faturas as $faturas){
                foreach ($faturas->cirurgiaFaturas as $f){
                    $f->delete();
                }
            }
        }

        if(file_exists($model->getPathFile().$model->arquivo)){
            unlink($model->getPathFile().$model->arquivo);
        }
        $model->delete();

        Yii::$app->session->setFlash("success", "Fatura removida com sucesso.");

        return $this->redirect(['index']);
    }

    public function actionReprocessar($id){
        $model = $this->findModel($id);

        if(file_exists($model->getPathFile().$model->arquivo)){
            $faturaModel = new Fatura();

            foreach ($model->faturas as $faturas){
                if($model->empresaFatura != "C") {
                    foreach ($faturas->cirurgiaFaturas as $f){
                        $f->delete();
                    }
                }
                $faturas->delete();
            }

            if($model->empresaFatura == "C"){
                $compPagamento = explode("/", $model->competenciaFatura);
                $model->dataPagamento = date("t/m/Y", strtotime($compPagamento[1]."-".$compPagamento[0]."-01"));

                $faturaModel->importaFaturamento($model->getPathFile().$model->arquivo, $model, $model->dataPagamento);
            }else{
                $faturaModel->importaFaturamento($model->getPathFile().$model->arquivo, $model);
            }

            Yii::$app->session->setFlash("success", "Fatura reprocessada com sucesso.");
        }else{
            Yii::$app->session->setFlash("info", "Não é possível reprocessar a fatura, arquivo de importação não existe. Realize a importação novamente");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImportacaoFatura model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImportacaoFatura the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImportacaoFatura::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
