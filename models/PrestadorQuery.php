<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Prestador]].
 *
 * @see Prestador
 */
class PrestadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Prestador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Prestador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
