<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacotes".
 *
 * @property int $idPacotes
 * @property string $procedimento Procedimento
 * @property string $pacote Pacote
 * @property string $desc_pacote Descrição Pacote
 */
class Pacotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pacotes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['procedimento', 'pacote'], 'string', 'max' => 15],
            [['desc_pacote'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idPacotes' => 'Id Pacotes',
            'procedimento' => 'Procedimento',
            'pacote' => 'Pacote',
            'desc_pacote' => 'Descrição Pacote',
        ];
    }

    /**
     * {@inheritdoc}
     * @return PacotesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacotesQuery(get_called_class());
    }
}
