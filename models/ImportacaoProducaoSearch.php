<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ImportacaoProducao;

/**
 * ImportacaoProducaoSearch represents the model behind the search form about `app\models\ImportacaoProducao`.
 */
class ImportacaoProducaoSearch extends ImportacaoProducao
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idImportacaoProducao', 'idUsuario'], 'integer'],
            [['competenciaProducao', 'dataInicial', 'dataFinal', 'descricao', 'tipoImportacao'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ImportacaoProducao::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query->orderBy('competenciaProducao DESC');

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'idImportacaoProducao' => $this->idImportacaoProducao,
            'competenciaProducao' => $this->competenciaProducao != null ? '01/'.$this->competenciaProducao : $this->competenciaProducao,
            'dataInicial' => $this->dataInicial,
            'dataFinal' => $this->dataFinal,
            'idUsuario' => $this->idUsuario,
            'tipoImportacao' => $this->tipoImportacao
        ]);

        $query->andFilterWhere(['like', 'descricao', $this->descricao]);

        return $dataProvider;
    }
}
