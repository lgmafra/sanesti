<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CirurgiaFatura]].
 *
 * @see CirurgiaFatura
 */
class CirurgiaFaturaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CirurgiaFatura[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CirurgiaFatura|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
