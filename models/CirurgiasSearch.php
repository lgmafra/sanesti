<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cirurgias;
use app\models\Usuario;

/**
 * CirurgiasSearch represents the model behind the search form about `app\models\Cirurgias`.
 */
class CirurgiasSearch extends Cirurgias
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idCirurgias', 'atendimentoCirurgia', 'codPaciente', 'codigoCirurgia', 'codigoAnestesista', 'codMVPrestador', 'idImportacaoProducao', 'idUsuario'], 'integer'],
            [['empresaCirurgia', 'carteiraPaciente', 'nomePaciente', 'cidCirurgia', 'codigoCirurgiao', 'idPrestador', 'dataCirurgia', 'inicioCirurgia', 'fimCirurgia', 'descricaoCirurgia', 'tipoAnestesia', 'procedimento', 'profat', 'descricaoProcedimento', 'nomeConvenio', 'nomeCirurgiao', 'crmCirurgiao', 'nomeAnestesista', 'crmAnestesista', 'codigoConvenio', 'possuiPagamento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cirurgias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if(\Yii::$app->user->identity->getNivel() == Usuario::USER_LANC_PART){
            $query->filterWhere(['in', 'codigoConvenio', Usuario::CONVENIOS_PARTICULAR]);
        }
        
        $query->orderBy('empresaCirurgia, dataCirurgia, inicioCirurgia, nomePaciente');

        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idCirurgias' => $this->idCirurgias,
            'atendimentoCirurgia' => $this->atendimentoCirurgia,
            'codPaciente' => $this->codPaciente,
            'inicioCirurgia' => $this->inicioCirurgia,
            'fimCirurgia' => $this->fimCirurgia,
            'codigoCirurgia' => $this->codigoCirurgia,
            //'codigoCirurgiao' => $this->codigoCirurgiao,
            'codigoAnestesista' => $this->codigoAnestesista,
            //'idPrestador' => $this->idPrestador,
            'codMVPrestador' => $this->codMVPrestador,
            'idImportacaoProducao' => $this->idImportacaoProducao,
            'codAcomodacao' => $this->codAcomodacao,
            'idUsuario'=>$this->idUsuario,
        ]);
        
        if (!empty($this->codigoConvenio)) {
            $query->andFilterWhere(['in', 'codigoConvenio', $this->codigoConvenio]);
        }

        if(!empty($this->procedimento)){
            $query->andFilterWhere(['in', 'procedimento', $this->procedimento]);
        }

        if(!empty($this->codigoCirurgiao)){
            $query->andFilterWhere(['in', 'codigoCirurgiao', $this->codigoCirurgiao]);
        }

        if(!empty($this->idPrestador)){
            $query->andFilterWhere(['in', 'idPrestador', $this->idPrestador]);
        }

        if($this->idImportacaoProducao == null){
            $dataCirurgia = explode(" - ", $this->dataCirurgia);
            if(isset($dataCirurgia[0]) && isset($dataCirurgia[1])){
                $query->andFilterWhere(['>=', 'dataCirurgia', $dataCirurgia[0]]);
                $query->andFilterWhere(['<=', 'dataCirurgia', $dataCirurgia[1]]);
            }
        }else{
            $query->andFilterWhere(['dataCirurgia'=>$this->dataCirurgia]);
        }        

        $query->andFilterWhere(['ilike', 'empresaCirurgia', $this->empresaCirurgia])
            ->andFilterWhere(['ilike', 'carteiraPaciente', $this->carteiraPaciente])
            ->andFilterWhere(['ilike', 'nomePaciente', $this->nomePaciente])
            ->andFilterWhere(['ilike', 'cidCirurgia', $this->cidCirurgia])
            ->andFilterWhere(['ilike', 'descricaoCirurgia', $this->descricaoCirurgia])
            ->andFilterWhere(['ilike', 'tipoAnestesia', $this->tipoAnestesia])
            //->andFilterWhere(['ilike', 'procedimento', $this->procedimento])
            ->andFilterWhere(['ilike', 'profat', $this->profat])
            ->andFilterWhere(['ilike', 'descricaoProcedimento', $this->descricaoProcedimento])
            ->andFilterWhere(['ilike', 'nomeConvenio', $this->nomeConvenio])
            ->andFilterWhere(['ilike', 'nomeCirurgiao', $this->nomeCirurgiao])
            ->andFilterWhere(['ilike', 'crmCirurgiao', $this->crmCirurgiao])
            ->andFilterWhere(['ilike', 'nomeAnestesista', $this->nomeAnestesista])
            ->andFilterWhere(['ilike', 'crmAnestesista', $this->crmAnestesista])
            ->andFilterWhere(['ilike', 'acomodacao', $this->acomodacao])
            ->andFilterWhere(['ilike', 'planoConvenio', $this->planoConvenio]);
        
        if(\Yii::$app->user->identity->getNivel() == Usuario::USER_LANC_PART){
            $query->andFilterWhere(['in', 'codigoConvenio', Usuario::CONVENIOS_PARTICULAR]);
        }

        if($this->possuiPagamento == "S"){
            $query->joinWith("cirurgiaFatura", true, "JOIN")->joinWith("cirurgiaFatura.fatura", true, "JOIN")->exists();
        }else if($this->possuiPagamento == "N"){
            $query->andWhere("not exists (SELECT 1 from \"cirurgiaFatura\" cf join fatura f on f.\"idFatura\" = cf.\"idFatura\" where cf.\"idCirurgias\" = cirurgias.\"idCirurgias\")");
        }
        
        $query->orderBy("empresaCirurgia, dataCirurgia, nomePaciente");

        return $dataProvider;
    }
    
    public static function retornaValorFatura(Cirurgias $data){
        $valorFatura = CirurgiaFatura::findAll(['idCirurgias'=>$data->idCirurgias]);
        
        $valor = 0.00;
        
        foreach ($valorFatura as $f){
            $valor += !is_null($f->fatura) ? $f->fatura->valorFatura : 0.00;
        }
        
        return $valor;
    }
    
    public static function retornaValorGlosa(Cirurgias $data){
        $valorFatura = CirurgiaFatura::findAll(['idCirurgias'=>$data->idCirurgias]);
        
        $valor = 0.00;
        
        foreach($valorFatura as $f){
            $valor += !is_null($f->fatura) ? $f->fatura->valorGlosa : 0.00;
        }
        
        return $valor;
    }
}
