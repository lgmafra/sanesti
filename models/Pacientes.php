<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pacientes".
 *
 * @property int $idPacientes
 * @property int $codPaciente Código Paciente MV
 * @property string $nomePaciente Nome Paciente
 */
class Pacientes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pacientes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codPaciente', 'nomePaciente'], 'required'],
            [['codPaciente'], 'default', 'value' => null],
            [['codPaciente'], 'integer'],
            ["codPaciente", 'unique'],
            [['nomePaciente'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPacientes' => 'Id Pacientes',
            'codPaciente' => 'Código Paciente MV',
            'nomePaciente' => 'Nome Paciente',
        ];
    }

    /**
     * @inheritdoc
     * @return PacientesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PacientesQuery(get_called_class());
    }

    public function inserePacienteImportacao($codMVPaciente, $nomePaciente){
        if(self::find()->where(['codPaciente'=>$codMVPaciente])->one() == null) {
            unset($this->idPacientes);
            $this->isNewRecord = true;
            $this->codPaciente = $codMVPaciente;
            $this->nomePaciente = $nomePaciente;
            $this->save();
        }
    }
}
