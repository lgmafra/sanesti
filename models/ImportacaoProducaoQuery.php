<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ImportacaoProducao]].
 *
 * @see ImportacaoProducao
 */
class ImportacaoProducaoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ImportacaoProducao[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportacaoProducao|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
