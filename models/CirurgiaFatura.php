<?php

namespace app\models;

use moonland\phpexcel\Excel;
use Yii;
use DateTime;

/**
 * This is the model class for table "cirurgiaFatura".
 *
 * @property integer $idCirurgiaFatura
 * @property integer $boletim
 * @property string $dataCirurgia
 * @property string $procedimento
 * @property string $empresaFatura
 * @property integer $idCirurgias
 * @property integer $idFatura
 *
 * @property Cirurgias $Cirurgias
 * @property Fatura $Fatura
 */
class CirurgiaFatura extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cirurgiaFatura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boletim', 'idCirurgias', 'idFatura'], 'integer'],
            [['dataCirurgia', 'procedimento', 'empresaFatura', 'idCirurgias'], 'required'],
            [['dataCirurgia'], 'safe'],
            [['procedimento'], 'string', 'max' => 150],
            [['empresaFatura'], 'string', 'max' => 1],
            [['idCirurgias'], 'exist', 'skipOnError' => true, 'targetClass' => Cirurgias::className(), 'targetAttribute' => ['idCirurgias' => 'idCirurgias']],
            [['idFatura'], 'exist', 'skipOnError' => true, 'targetClass' => Fatura::className(), 'targetAttribute' => ['idFatura' => 'idFatura']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCirurgiaFatura' => 'Id Cirurgia Fatura',
            'boletim' => 'Boletim',
            'dataCirurgia' => 'Data Cirurgia',
            'procedimento' => 'Procedimento',
            'empresaFatura' => 'Empresa Fatura',
            'idCirurgias' => 'Cirurgia',
            'idFatura' => 'Fatura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCirurgias()
    {
        return $this->hasOne(Cirurgias::className(), ['idCirurgias' => 'idCirurgias']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFatura()
    {
        return $this->hasOne(Fatura::className(), ['idFatura' => 'idFatura']);
    }

    /**
     * @inheritdoc
     * @return CirurgiaFaturaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CirurgiaFaturaQuery(get_called_class());
    }
    
    public function buscaCirurgiaFatura($boletim, $procedimento, $data, $procedimentos_array){
        if($boletim == "16430695"){
            $a = true;
        }
        $cirurgiaFatura = $this->find()->where([
            'boletim'=>$boletim,
            'procedimento'=>$procedimento,
            'dataCirurgia'=>DateTime::createFromFormat('d/m/Y', $data)->format('Y-m-d')
        ])->andWhere(['is not', 'idCirurgias', null])->one();

        if($cirurgiaFatura == null){
            if(!empty($procedimentos_array)){
                $cirurgiaFatura = $this->find()->where([
                    'boletim'=>$boletim,
                    'dataCirurgia'=>DateTime::createFromFormat('d/m/Y', $data)->format('Y-m-d')
                ])->andWhere(['in', 'procedimento', $procedimentos_array])
                ->andWhere(['is not', 'idCirurgias', null])->one();

                if($cirurgiaFatura == null){
                    $cirurgiaFatura = $this->find()->where([
                        'boletim'=>$boletim
                    ])->andWhere(['is not', 'idCirurgias', null])->one();
                }
            }
        }

        return $cirurgiaFatura;
    }
    
    public function buscaBoletimCirurgia($boletim, $data){
        return $this->find()->where([
            'boletim'=>$boletim,
            //'dataCirurgia'=>DateTime::createFromFormat('d/m/y', $data)->format('Y-m-d')
        ])->all();
    }

    public function validaBoletins($caminhoArquivo){
        $dadosImportar = Excel::widget([
            'mode' => 'import',
            'fileName'=>$caminhoArquivo,
            'setFirstRecordAsKeys'=>false
        ]);

        foreach ($dadosImportar as $index=>$value){
            if($index > 2){
                $dados = $this->find()->where(['boletim'=>$value["A"]])->all();

                foreach ($dados as $d){
                    if($d->cirurgias != null){
                        Cirurgias::updateAll(['boletim_valido'=>true], ["idCirurgias"=>$d->cirurgias->idCirurgias]);
                    }
                }
            }
        }
    }
}
