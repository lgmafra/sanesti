<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "procedimentos".
 *
 * @property int $idProcedimentos
 * @property string $procedimento Procedimento
 * @property string $profat TUSS
 * @property string $descricaoProcedimento Descricao Procedimento
 */
class Procedimentos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'procedimentos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['procedimento'], 'required'],
            [['procedimento', 'profat'], 'string', 'max' => 20],
            [['descricaoProcedimento'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProcedimentos' => 'Id Procedimentos',
            'procedimento' => 'Procedimento',
            'profat' => 'TUSS',
            'descricaoProcedimento' => 'Descricao Procedimento',
        ];
    }

    /**
     * @inheritdoc
     * @return ProcedimentosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProcedimentosQuery(get_called_class());
    }

    public function insereProceimentoImportacao($procedimento, $profat, $descricaoProcedimento){
        if(self::find()->where(['procedimento'=>$procedimento])->one() == null) {
            unset($this->idProcedimentos);
            $this->isNewRecord = true;
            $this->procedimento = $procedimento;
            $this->profat = $profat;
            $this->descricaoProcedimento = $descricaoProcedimento;
            $this->save();
        }
    }
}
