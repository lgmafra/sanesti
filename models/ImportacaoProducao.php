<?php

namespace app\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\base\Exception;

/**
 * This is the model class for table "importacaoProducao".
 *
 * @property integer $idImportacaoProducao
 * @property string $competenciaProducao
 * @property string $dataInicial
 * @property string $dataFinal
 * @property string $descricao
 * @property string $tipoImportacao
 * @property integer $idUsuario
 * @property string $arquivo
 *
 * @property Cirurgias[] $Cirurgias
 * @property Usuario $Usuario
 */
class ImportacaoProducao extends ActiveRecord
{
    public $_arquivoImportacao;
    
    public function behaviors() {
        return [
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_AFTER_FIND=>[
                        'competenciaProducao'=>function(){},
                        'dataInicial'=>function(){},
                        'dataFinal'=>function(){}
                    ],
                ],
                'value'=>function($event){
                    $this->competenciaProducao = Yii::$app->formatter->asDate($this->competenciaProducao, "php:m/Y");
                    $this->dataInicial = Yii::$app->formatter->asDate($this->dataInicial, "php:d/m/Y");
                    $this->dataFinal = Yii::$app->formatter->asDate($this->dataFinal, "php:d/m/Y");
                    return $event;
                }
            ],
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_BEFORE_INSERT=>[
                        'competenciaProducao'=>function(){},
                    ]
                ],
                'value'=>function($event){
                    $this->competenciaProducao = "01/".$this->competenciaProducao;
                    return $event;
                }
            ],
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_BEFORE_UPDATE=>[
                        'competenciaProducao'=>function(){},
                    ],
                ],
                'value'=>function($event){
                    $this->competenciaProducao = "01/".$this->competenciaProducao;
                    return $event;
                }
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'importacaoProducao';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competenciaProducao', 'dataInicial', 'dataFinal', 'descricao', 'tipoImportacao'], 'required'],
            ['idUsuario', 'default', 'value'=>  Yii::$app->user->getId()],
            [['competenciaProducao', 'dataInicial', 'dataFinal'], 'safe'],
            [['idUsuario'], 'integer'],
            ['tipoImportacao', 'string', 'max'=>1],
            //[['dataInicial', 'dataFinal'], 'date'],
            ['_arquivoImportacao', 'required'],
            [['_arquivoImportacao'], 'file', 'extensions'=>'xls, xlsx'],
            [['descricao'], 'string', 'max' => 100],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'idUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idImportacaoProducao' => 'Id Importacao Producao',
            'competenciaProducao' => 'Competência Produção',
            'dataInicial' => 'Data Inicial',
            'dataFinal' => 'Data Final',
            'descricao' => 'Descrição',
            'idUsuario' => 'Usuário Importou',
            '_arquivoImportacao' => 'Arquivo para Importação',
            'arquivo' => 'Arquivo Importação',
            'tipoImportacao' => 'Tipo Importação'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCirurgias()
    {
        return $this->hasMany(Cirurgias::className(), ['idImportacaoProducao' => 'idImportacaoProducao']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @inheritdoc
     * @return ImportacaoProducaoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImportacaoProducaoQuery(get_called_class());
    }
    
    public function getPathFile(){
        return Yii::getAlias("@webroot")."/uploads/";
    }
    
    public function getNameFile($extension){
        $comp = explode("/", $this->competenciaProducao);
        return date("Y").date("m").date("H").date("i")."_prod_".$comp[1]."_".$comp[0].".".$extension;
    }
    
    public function validaCompetenciaImportada(){
        if(ImportacaoProducao::find()->where(['competenciaProducao'=>'01/'.$this->competenciaProducao, 'tipoImportacao'=>$this->tipoImportacao])->exists()){
            throw new Exception("Produção já importada. Escolha outra competência.");
        }
    }
}
