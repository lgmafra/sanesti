<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Procedimentos]].
 *
 * @see Procedimentos
 */
class ProcedimentosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Procedimentos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Procedimentos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
