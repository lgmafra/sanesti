<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Prestador;

/**
 * PrestadorSearch represents the model behind the search form about `app\models\Prestador`.
 */
class PrestadorSearch extends Prestador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idPrestador', 'codMVPrestador'], 'integer'],
            [['nomePrestador', 'crmPrestador', 'emailPrestador'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Prestador::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination'=>[
                'pageSize'=>20
            ]
        ]);
        
        $query->orderBy("nomePrestador");

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idPrestador' => $this->idPrestador,
            'codMVPrestador' => $this->codMVPrestador,
        ]);

        $query->andFilterWhere(['like', 'nomePrestador', $this->nomePrestador])
            ->andFilterWhere(['like', 'crmPrestador', $this->crmPrestador])
            ->andFilterWhere(['like', 'emailPrestador', $this->emailPrestador]);

        return $dataProvider;
    }
}
