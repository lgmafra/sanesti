<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Pacotes]].
 *
 * @see Pacotes
 */
class PacotesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Pacotes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Pacotes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
