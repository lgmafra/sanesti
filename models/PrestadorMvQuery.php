<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PrestadorMv]].
 *
 * @see PrestadorMv
 */
class PrestadorMvQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PrestadorMv[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PrestadorMv|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
