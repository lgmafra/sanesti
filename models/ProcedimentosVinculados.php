<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "procedimentosVinculados".
 *
 * @property int $idProcedimentosVinculados
 * @property string $procedimento Procedimento
 * @property string $procedimento_vinculado Procedimento Vinculado
 * @property string $desc_proc_vinculado Descrição Procedimento
 */
class ProcedimentosVinculados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'procedimentosVinculados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['procedimento', 'procedimento_vinculado'], 'string', 'max' => 15],
            [['desc_proc_vinculado'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idProcedimentosVinculados' => 'Id Procedimentos Vinculados',
            'procedimento' => 'Procedimento',
            'procedimento_vinculado' => 'Procedimento Vinculado',
            'desc_proc_vinculado' => 'Descrição Procedimento',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ProcedimentosVinculadosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProcedimentosVinculadosQuery(get_called_class());
    }
}
