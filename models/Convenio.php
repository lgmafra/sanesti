<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "convenio".
 *
 * @property int $idConvenio
 * @property int $codigoConvenio
 * @property string $nomeConvenio
 */
class Convenio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'convenio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoConvenio', 'nomeConvenio'], 'required'],
            [['codigoConvenio'], 'default', 'value' => null],
            [['codigoConvenio'], 'integer'],
            [['nomeConvenio'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idConvenio' => 'Id Convenio',
            'codigoConvenio' => 'Codigo Convenio',
            'nomeConvenio' => 'Nome Convenio',
        ];
    }

    /**
     * @inheritdoc
     * @return ConvenioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConvenioQuery(get_called_class());
    }
}
