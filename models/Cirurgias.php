<?php

namespace app\models;

use Yii;
use moonland\phpexcel\Excel;
use yii\base\Exception;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use DateTime;

/**
 * This is the model class for table "cirurgias".
 *
 * @property integer $idCirurgias
 * @property string $empresaCirurgia
 * @property integer $atendimentoCirurgia
 * @property string $carteiraPaciente
 * @property integer $codPaciente
 * @property string $nomePaciente
 * @property string $cidCirurgia
 * @property string $dataCirurgia
 * @property string $inicioCirurgia
 * @property string $fimCirurgia
 * @property integer $codigoCirurgia
 * @property string $descricaoCirurgia
 * @property string $tipoAnestesia
 * @property string $procedimento
 * @property string $profat
 * @property string $descricaoProcedimento
 * @property integer $codigoConvenio
 * @property string $nomeConvenio
 * @property string $planoConvenio
 * @property integer $codAcomodacao
 * @property string $acomodacao
 * @property integer $codigoCirurgiao
 * @property string $nomeCirurgiao
 * @property string $crmCirurgiao
 * @property integer $codigoAnestesista
 * @property string $nomeAnestesista
 * @property string $crmAnestesista
 * @property integer $idPrestador
 * @property integer $codMVPrestador
 * @property integer $idImportacaoProducao
 * @property integer $idUsuario
 * @property integer $codAvisoCirurgia
 * @property integer $codCirurgiaAviso
 * @property string $tipoImportacao
 * @property boolean $boletim_valido
 *
 * @property ImportacaoProducao $ImportacaoProducao
 * @property Prestador $Prestador
 * @property CirurgiaFatura[] $CirurgiaFatura
 * @property Usuario $Usuario
 */
class Cirurgias extends ActiveRecord
{
    public $_aplicarBoletim;
    public $mes;
    public $qtdcirurgias;
    public $qtdconsultas;
    public $totalPago;
    public $boletim;
    public $possuiPagamento;
    public $_arquivoImportacao;

    public function behaviors() {
        return [
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_AFTER_FIND=>[
                        'dataCirurgia'=>function(){},
                    ],
                ],
                'value'=>function($event){
                    $this->dataCirurgia = Yii::$app->formatter->asDate($this->dataCirurgia, "php:d/m/Y");
                    return $event;
                }
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cirurgias';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empresaCirurgia', 'atendimentoCirurgia', 'codPaciente', 'nomePaciente', 'cidCirurgia', 'dataCirurgia', 'inicioCirurgia', 'fimCirurgia', 'codigoCirurgia', 'descricaoCirurgia', 'tipoAnestesia', 'procedimento', 'codigoConvenio', 'nomeConvenio', 'codigoCirurgiao', 'nomeCirurgiao', 'codigoAnestesista', 'nomeAnestesista', 'idPrestador', 'codMVPrestador', 'idImportacaoProducao'], 'required'],
            [['atendimentoCirurgia', 'codPaciente', 'codigoCirurgia', 'codigoConvenio', 'codigoCirurgiao', 'codigoAnestesista', 'idPrestador', 'codMVPrestador', 'idImportacaoProducao', 'codAcomodacao', 'idUsuario', 'codAvisoCirurgia', 'codCirurgiaAviso'], 'integer'],
            [['dataCirurgia', 'inicioCirurgia', 'fimCirurgia', 'procedimento'], 'safe'],
            [['empresaCirurgia'], 'string', 'max' => 10],
            [['_arquivoImportacao'], 'required'],
            //[['dataCirurgia'], 'date'],
            //['dataCirurgia', 'compare', 'compareAttribute'=>'ImportacaoProducao.dataInicial', 'operator'=>'<', 'enableClientValidation' => false, 'message'=>'Data inválida'],
            //['dataCirurgia', 'compare', 'compareAttribute'=>'ImportacaoProducao.dataFinal', 'operator'=>'>', 'enableClientValidation' => false, 'message'=>'Data inválida'],
            [['carteiraPaciente', 'cidCirurgia', 'profat', 'crmCirurgiao', 'crmAnestesista'], 'string', 'max' => 20],
            [['nomePaciente', 'tipoAnestesia', 'planoConvenio', 'acomodacao'], 'string', 'max' => 100],
            [['nomeConvenio', 'nomeCirurgiao', 'nomeAnestesista'], 'string', 'max' => 70],
            [['descricaoCirurgia', 'descricaoProcedimento'], 'string', 'max' => 255],
            ['tipoImportacao', 'string', 'max'=>1],
            ['_aplicarBoletim', 'boolean'],
            ['boletim', 'string'],
            [['idImportacaoProducao'], 'exist', 'skipOnError' => true, 'targetClass' => ImportacaoProducao::className(), 'targetAttribute' => ['idImportacaoProducao' => 'idImportacaoProducao']],
            [['idPrestador'], 'exist', 'skipOnError' => true, 'targetClass' => Prestador::className(), 'targetAttribute' => ['idPrestador' => 'idPrestador']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'idUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idCirurgias' => 'Id Cirurgias',
            'empresaCirurgia' => 'Empresa',
            'atendimentoCirurgia' => 'Atendimento',
            'carteiraPaciente' => 'Carteira',
            'codPaciente' => 'Código Paciente',
            'nomePaciente' => 'Nome Paciente',
            'cidCirurgia' => 'CID',
            'dataCirurgia' => 'Data Cirurgia',
            'inicioCirurgia' => 'Hora Inicio',
            'fimCirurgia' => 'Hora Termino',
            'codigoCirurgia' => 'Cirurgia',
            'descricaoCirurgia' => 'Descrição Cirurgia',
            'tipoAnestesia' => 'Tipo Anestesia',
            'procedimento' => 'Procedimento',
            'profat' => 'TUSS',
            'descricaoProcedimento' => 'Descricao Procedimento',
            'codigoConvenio' => 'Código Convênio',
            'nomeConvenio' => 'Nome Convênio',
            'planoConvenio' => 'Plano Convênio',
            'codAcomodacao' => 'Código Acomodação',
            'acomodacao' => 'Acomodação',
            'codigoCirurgiao' => 'Cirurgião',
            'nomeCirurgiao' => 'Nome Cirurgião',
            'crmCirurgiao' => 'CRM Cirurgião',
            'codigoAnestesista' => 'Código Anestesista',
            'nomeAnestesista' => 'Nome Anestesista',
            'crmAnestesista' => 'CRM Anestesista',
            'idPrestador' => 'Anestesista',
            'codMVPrestador' => 'Código MV Prestador',
            'idImportacaoProducao' => 'Id Importacao Producao',
            '_aplicarBoletim'=>'Aplicar Boletim a Todos os procedimentos desse atendimento',
            'idUsuario'=>'Usuário Alterou'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacaoProducao()
    {
        return $this->hasOne(ImportacaoProducao::className(), ['idImportacaoProducao' => 'idImportacaoProducao']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestador()
    {
        return $this->hasOne(Prestador::className(), ['idPrestador' => 'idPrestador']);
    }
    
    public function getCirurgiaFatura()
    {
        return $this->hasMany(CirurgiaFatura::className(), ['idCirurgias' => 'idCirurgias']);
    }
    
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario'=>'idUsuario']);
    }

    /**
     * @inheritdoc
     * @return CirurgiasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CirurgiasQuery(get_called_class());
    }
    
    public function importaProducao($caminhoArquivo, ImportacaoProducao $modelImportacao){
        $dadosImportar = Excel::widget([
            'mode' => 'import', 
            'fileName'=>$caminhoArquivo,
            'setFirstRecordAsKeys'=>false
        ]);

        $modelPaciente = new Pacientes();
        $modelProcedimento = new Procedimentos();
        
        $dataInicial = DateTime::createFromFormat('d/m/Y', $modelImportacao->dataInicial);
        $dataFinal = DateTime::createFromFormat('d/m/Y', $modelImportacao->dataFinal);
        
        foreach ($dadosImportar as $index=>$dados){
            if($index > 3){
                $cirurgiaCadastrada = $this->findOne([
                    'atendimentoCirurgia'=>$dados["B"],
                    'procedimento'=>$dados["P"],
                    'dataCirurgia'=>$dados["F"],
                    'codAvisoCirurgia'=>$dados["AB"],
                    'codCirurgiaAviso'=>$dados["AC"]
                ]);

                if($cirurgiaCadastrada == null){
                    $cirurgia = new Cirurgias();
                    $cirurgia->isNewRecord = true;
                    unset($cirurgia->idCirurgias);
                }else{
                    $cirurgia = $cirurgiaCadastrada;
                    $cirurgia->isNewRecord = false;
                }

                $cirurgia->empresaCirurgia = $dados["A"]; // =>string(7) "Empresa"
                $cirurgia->atendimentoCirurgia = $dados["B"]; // =>string(11) "Atendimento"
                $cirurgia->cidCirurgia = !empty($dados["C"]) ? $dados["C"] : "NÃO INFORMADO"; // =>string(3) "CID"
                $cirurgia->codAcomodacao = $dados["D"]; // =>string(7) "Código"
                $cirurgia->acomodacao = $dados["E"]; // =>string(12) "Acomodação"
                $cirurgia->dataCirurgia = $dados["F"]; // =>string(4) "Data"
                $cirurgia->inicioCirurgia = $dados["G"]; // =>string(7) "Início"
                $cirurgia->fimCirurgia = $dados["H"]; // =>string(8) "Término"
                $cirurgia->codPaciente = $dados["I"]; // =>string(7) "Código"
                $cirurgia->nomePaciente = $dados["J"]; // =>string(8) "Paciente"
                $cirurgia->carteiraPaciente = $dados["L"]; // =>string(8) "Carteira"
                $cirurgia->codigoCirurgia = $dados["M"]; // =>string(7) "Código"
                $cirurgia->descricaoCirurgia = $dados["N"]; // =>string(8) "Cirurgia"
                $cirurgia->tipoAnestesia = !empty($dados["O"]) ? $dados["O"] : "NÃO INFORMADO"; // =>string(14) "Tipo Anestesia"
                $cirurgia->procedimento = !empty($dados["P"]) ? $dados["P"] : "NÃO INFORMADO"; // =>string(12) "Procedimento"
                $cirurgia->profat = $dados["Q"]; // =>string(7) "Código"

                if($dados["S"] == "1" || $dados["S"] == "101"){ //Se convenio for sus, o procedimento é o nome da cirurgia
                    $descProcedimento = $dados["N"]; // =>string(4) "TUSS"
                }else{
                    $descProcedimento = $dados["R"]; // =>string(4) "TUSS"
                }
                $cirurgia->descricaoProcedimento = $descProcedimento;

                $cirurgia->codigoConvenio = $dados["S"]; // =>string(7) "Código"
                $cirurgia->nomeConvenio = $dados["T"]; // =>string(9) "Convênio"
                $cirurgia->planoConvenio = $dados["U"]; // =>string(5) "Plano"
                $cirurgia->codigoCirurgiao = $dados["V"]; // =>string(7) "Código"
                $cirurgia->nomeCirurgiao = $dados["W"]; // =>string(9) "Prestador"
                $cirurgia->crmCirurgiao = $dados["X"]; // =>string(3) "CRM"
                $cirurgia->codigoAnestesista = $dados["Y"]; // =>string(7) "Código"
                $cirurgia->nomeAnestesista = $dados["Z"]; // =>string(11) "Anestesista"
                $cirurgia->crmAnestesista = $dados["AA"]; // =>string(3) "CRM"
                
                $cirurgia->codAvisoCirurgia = $dados["AB"];
                $cirurgia->codCirurgiaAviso = $dados["AC"];

                $modelPaciente->inserePacienteImportacao($dados["I"], $dados["J"]);
                if(!empty($dados["P"])){
                    $modelProcedimento->insereProceimentoImportacao($dados["P"], $dados["Q"], $descProcedimento);
                }

                $dadosPrestador = Prestador::findOne(['codMVPrestador'=>$cirurgia->codigoAnestesista]);
                if($dadosPrestador !== null){
                    $cirurgia->idPrestador = $dadosPrestador->idPrestador;
                    $cirurgia->codMVPrestador = $dadosPrestador->codMVPrestador;
                }

                $cirurgia->idImportacaoProducao = $modelImportacao->idImportacaoProducao;
                $cirurgia->idUsuario = \Yii::$app->user->getId();
                $cirurgia->tipoImportacao = "P";

                if(DateTime::createFromFormat('d/m/Y', $cirurgia->dataCirurgia)->format('Y-m-d') < $dataInicial->format('Y-m-d') 
                   || DateTime::createFromFormat('d/m/Y', $cirurgia->dataCirurgia)->format('Y-m-d') > $dataFinal->format('Y-m-d')){
                    throw new Exception("O atendimento <b>{$cirurgia->atendimentoCirurgia} - {$cirurgia->dataCirurgia}</b> está fora do período informado.");
                }

                $cirurgia->save(false);
            }
        }
    }

    public function importaConsultas($caminhoArquivo, ImportacaoProducao $modelImportacao){
        $dadosImportar = Excel::widget([
            'mode' => 'import',
            'fileName'=>$caminhoArquivo,
            'setFirstRecordAsKeys'=>false
        ]);

        $modelPaciente = new Pacientes();
        $modelProcedimento = new Procedimentos();

        $dataInicial = DateTime::createFromFormat('d/m/Y', $modelImportacao->dataInicial);
        $dataFinal = DateTime::createFromFormat('d/m/Y', $modelImportacao->dataFinal);

        foreach ($dadosImportar as $index=>$dados){
            if($index > 3){
                $cirurgiaCadastrada = $this->findOne([
                    'atendimentoCirurgia'=>$dados["A"],
                    'procedimento'=>$dados["F"],
                    'dataCirurgia'=>$dados["B"]
                ]);

                if($cirurgiaCadastrada == null){
                    $cirurgia = new Cirurgias();
                    $cirurgia->isNewRecord = true;
                    unset($cirurgia->idCirurgias);
                }else{
                    $cirurgia = $cirurgiaCadastrada;
                    $cirurgia->isNewRecord = false;
                }

                $cirurgia->empresaCirurgia = "HCMF"; // =>string(7) "Empresa"
                $cirurgia->atendimentoCirurgia = $dados["A"]; // =>string(11) "Atendimento"
                $cirurgia->cidCirurgia = "NÃO INFORMADO"; // =>string(3) "CID"
                $cirurgia->codAcomodacao = "NÃO INFORMADO"; // =>string(7) "Código"
                $cirurgia->acomodacao = "NÃO INFORMADO"; // =>string(12) "Acomodação"
                $cirurgia->dataCirurgia = $dados["B"]; // =>string(4) "Data"
                $cirurgia->inicioCirurgia = $dados["C"]; // =>string(7) "Início"
                $cirurgia->fimCirurgia = $dados["C"]; // =>string(8) "Término"
                $cirurgia->codPaciente = $dados["D"]; // =>string(7) "Código"
                $cirurgia->nomePaciente = $dados["E"]; // =>string(8) "Paciente"
                $cirurgia->carteiraPaciente = "NÃO INFORMADO";//$dados["L"]; // =>string(8) "Carteira"
                $cirurgia->codigoCirurgia = 888888; // =>string(7) "Código"
                $cirurgia->descricaoCirurgia = "Consulta"; // =>string(8) "Cirurgia"
                $cirurgia->tipoAnestesia = "NÃO INFORMADO"; // =>string(14) "Tipo Anestesia"
                $cirurgia->procedimento = !empty($dados["F"]) ? $dados["F"] : "NÃO INFORMADO"; // =>string(12) "Procedimento"
                $cirurgia->profat = $dados["F"]; // =>string(7) "Código"
                $cirurgia->descricaoProcedimento = $dados["G"]; // =>string(4) "TUSS"

                $cirurgia->codigoConvenio = $dados["I"]; // =>string(7) "Código"
                $cirurgia->nomeConvenio = $dados["J"]; // =>string(9) "Convênio"
                //$cirurgia->planoConvenio = $dados["U"]; // =>string(5) "Plano"
                $cirurgia->codigoCirurgiao = 0;//$dados["V"]; // =>string(7) "Código"
                $cirurgia->nomeCirurgiao = "NÃO INFORMADO";//$dados["W"]; // =>string(9) "Prestador"
                $cirurgia->crmCirurgiao = "NÃO INFORMADO";//$dados["X"]; // =>string(3) "CRM"
                $cirurgia->codigoAnestesista = $dados["M"]; // =>string(7) "Código"
                $cirurgia->nomeAnestesista = $dados["N"]; // =>string(11) "Anestesista"
                //$cirurgia->crmAnestesista = $dados["AA"]; // =>string(3) "CRM"

                $modelPaciente->inserePacienteImportacao($dados["D"], $dados["E"]);
                if(!empty($dados["F"])){
                    $modelProcedimento->insereProceimentoImportacao($dados["F"], $dados["F"], $dados["G"]);
                }

                $dadosPrestador = Prestador::findOne(['codMVPrestador'=>$cirurgia->codigoAnestesista]);
                if($dadosPrestador !== null){
                    $cirurgia->idPrestador = $dadosPrestador->idPrestador;
                    $cirurgia->codMVPrestador = $dadosPrestador->codMVPrestador;
                }

                $cirurgia->idImportacaoProducao = $modelImportacao->idImportacaoProducao;
                $cirurgia->idUsuario = \Yii::$app->user->getId();
                $cirurgia->tipoImportacao = "C";

                if(DateTime::createFromFormat('d/m/Y', $cirurgia->dataCirurgia)->format('Y-m-d') < $dataInicial->format('Y-m-d')
                    || DateTime::createFromFormat('d/m/Y', $cirurgia->dataCirurgia)->format('Y-m-d') > $dataFinal->format('Y-m-d')){
                    throw new Exception("A consulta <b>{$cirurgia->atendimentoCirurgia} - {$cirurgia->dataCirurgia}</b> está fora do período informado.");
                }

                $cirurgia->save(false);
            }
        }
    }
    
    public function getDataByMonth(){
        $retorno = [];

        $retorno['spline']['name'] = 'Produção';
        $retorno['series']['name'][] = 'Cirurgias';
        $retorno['series']['name'][] = 'Consultas';
        $retorno['series']['name'][] = 'Pagamentos';

        $retorno['categorias'] = [];
        $retorno['series']['data'] = [];
        
        $competencias = ImportacaoProducao::find()->distinct()->select('competenciaProducao')->limit(12)->orderBy('competenciaProducao DESC')->all();

        $competencias = array_reverse($competencias);

        foreach ($competencias as $c){
            $comp = \DateTime::createFromFormat('d/m/Y', '01/'.$c->competenciaProducao)->format('Y-m');
            $retorno['categorias'][] = $c->competenciaProducao;

            $queryCirurgias = $this->find()->select(['count(*) qtdcirurgias'])->where([
                'to_char("dataCirurgia", \'YYYY-MM\')'=>$comp,
                'tipoImportacao'=>'P'
            ])->one();

            $queryConsultas = $this->find()->select(['count(*) qtdconsultas'])->where([
                'to_char("dataCirurgia", \'YYYY-MM\')'=>$comp,
                'tipoImportacao'=>'C'
            ])->one();

            $queryPagamentos = $this->find()
                    ->joinWith("cirurgiaFatura")
                    ->joinWith("cirurgiaFatura.fatura")
                    ->select(['(sum("valorPago")-sum("valorGlosa")) "totalPago"'])
                    ->where([
                        'to_char(cirurgias."dataCirurgia", \'YYYY-MM\')'=>$comp
                    ])->one();

            $retorno['series']['cirurgias'][] = $queryCirurgias->qtdcirurgias;
            $retorno['series']['consultas'][] = $queryConsultas->qtdconsultas;
            $retorno['series']['valorpago'][] = (float)$queryPagamentos->totalPago;
            $retorno['spline']['producao'][] = $queryCirurgias->qtdcirurgias+$queryConsultas->qtdconsultas;
        }

        return $retorno;
    }
    
    public function retornaValorCampo($campoBusca, $campoRetorno, $chaveBusca){
        $retorno = $this->find()->select($campoRetorno)->where([$chaveBusca=>$campoBusca])->one();
        return $retorno;
    }

    public function getPathFile(){
        return Yii::getAlias("@webroot")."/uploads/";
    }

    public function getNameFile($extension){
        return date("Y").date("m").date("H").date("i")."_boletins_enviados.".$extension;
    }
}
