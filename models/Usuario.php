<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property integer $idUsuario
 * @property string $nomeUsuario
 * @property string $loginUsuario
 * @property string $emailUsuario
 * @property string $senhaUsuario
 * @property boolean $statusUsuario
 * @property integer $idPrestador
 * @property integer $idNivel
 *
 * @property ImportacaoProducao[] $ImportacaoProducao
 * @property Prestador $Prestador
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    
    const USER_ADMIN = 1;
    const USER_LANC_PART = 2;
    
    const NIVEL_USER = [
        self::USER_ADMIN => 'Administrador',
        self::USER_LANC_PART => 'Lançamento Particular'
    ];
    const CONVENIOS_PARTICULAR = [
        40, 85
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nomeUsuario', 'loginUsuario', 'emailUsuario', 'idNivel'], 'required'],
            ['senhaUsuario', 'required', 'on'=>'create'],
            [['statusUsuario'], 'boolean'],
            [['idPrestador', 'idNivel'], 'integer'],
            [['nomeUsuario'], 'string', 'max' => 70],
            [['loginUsuario'], 'string', 'max' => 60],
            ['loginUsuario', 'filter', 'filter'=>function($value){return str_replace(' ', '', $value);}],
            [['emailUsuario', 'senhaUsuario'], 'string', 'max' => 100],
            [['emailUsuario'], 'unique'],
            [['loginUsuario'], 'unique'],
            [['idPrestador'], 'exist', 'skipOnError' => true, 'targetClass' => Prestador::className(), 'targetAttribute' => ['idPrestador' => 'idPrestador']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsuario' => 'Id Usuario',
            'nomeUsuario' => 'Nome Usuário',
            'loginUsuario' => 'Login',
            'emailUsuario' => 'Email',
            'senhaUsuario' => 'Senha',
            'statusUsuario' => 'Ativo?',
            'idPrestador' => 'Prestador',
            'idNivel'=>'Nível'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacaoProducaos()
    {
        return $this->hasMany(ImportacaoProducao::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrestador()
    {
        return $this->hasOne(Prestador::className(), ['idPrestador' => 'idPrestador']);
    }

    /**
     * @inheritdoc
     * @return UsuarioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsuarioQuery(get_called_class());
    }

    public function getAuthKey() {
        
    }
    
    public function getNome(){
        return $this->nomeUsuario;
    }

    public function getId() {
        return $this->idUsuario;
    }
    
    public function getNivel(){
        return $this->idNivel;
    }
    
    public function getIsUserAdmin(){
        return self::USER_ADMIN == $this->idNivel;
    }
    
    public function getIdUserLancPart(){
        return self::USER_LANC_PART == $this->idNivel;
    }

    public function validateAuthKey($authKey) {
        
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        
    }
    
    public static function doLogin($login, $senha){
        $usuario = static::findOne(['loginUsuario'=>$login, 'senhaUsuario'=>  sha1($senha)]);
        
        if(!empty($usuario)){
            return $usuario;
        }
        return null;
    }

}
