<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fatura;

/**
 * FaturaSearch represents the model behind the search form about `app\models\Fatura`.
 */
class FaturaSearch extends Fatura
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idFatura', 'idImportacaoFatura'], 'integer'],
            [['valorFatura', 'valorGlosa', 'valorPago'], 'number'],
            [['tipoPagamento'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fatura::find();
        
        $query->joinWith(['cirurgiaFaturas']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idFatura' => $this->idFatura,
            'valorFatura' => $this->valorFatura,
            'valorGlosa' => $this->valorGlosa,
            'valorPago' => $this->valorPago,
            'idImportacaoFatura' => $this->idImportacaoFatura,
        ]);
        
        $query->andFilterWhere(['like', 'tipoPagamento', $this->tipoPagamento]);

        return $dataProvider;
    }
}
