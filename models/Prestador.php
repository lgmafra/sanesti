<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestador".
 *
 * @property integer $idPrestador
 * @property integer $codMVPrestador
 * @property string $nomePrestador
 * @property string $crmPrestador
 * @property string $emailPrestador
 *
 * @property Cirurgias[] $Cirurgias
 * @property Usuario[] $Usuarios
 */
class Prestador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prestador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codMVPrestador', 'crmPrestador', 'emailPrestador', 'nomePrestador'], 'required'],
            [['codMVPrestador'], 'integer'],
            [['nomePrestador'], 'string', 'max' => 70],
            [['crmPrestador'], 'string', 'max' => 20],
            [['emailPrestador'], 'string', 'max' => 100],
            [['crmPrestador'], 'unique'],
            [['emailPrestador'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPrestador' => 'Id Prestador',
            'codMVPrestador' => 'Código MV',
            'nomePrestador' => 'Nome Prestador',
            'crmPrestador' => 'CRM Prestador',
            'emailPrestador' => 'Email Prestador',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCirurgias()
    {
        return $this->hasMany(Cirurgias::className(), ['idPrestador' => 'idPrestador']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['idPrestador' => 'idPrestador']);
    }

    /**
     * @inheritdoc
     * @return PrestadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrestadorQuery(get_called_class());
    }
}
