<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'username' => 'Login',
            'password' => 'Senha'
        ];
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $usuario = Usuario::doLogin($this->username, $this->password);
            if (!is_null($usuario)) {
                if ($usuario->statusUsuario) {
                    return Yii::$app->user->login($usuario, $this->rememberMe ? 3600 * 24 * 30 : 0);
                }else{
                    $this->addErrors(['username' => '', 'password' => 'Usuário inatívo. Entre em contato com o administrador']);
                }
            } else {
                $this->addErrors(['username' => '', 'password' => 'Usuário ou senha inválido.']);
            }
        }
        return false;
    }
}
