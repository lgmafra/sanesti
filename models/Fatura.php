<?php

namespace app\models;

use Yii;
use moonland\phpexcel\Excel;
use app\models\Cirurgias;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\behaviors\AttributeBehavior;
use DateTime;

/**
 * This is the model class for table "fatura".
 *
 * @property integer $idFatura
 * @property string $valorFatura
 * @property string $valorGlosa
 * @property string $valorPago
 * @property string $tipoPagamento
 * @property integer $idImportacaoFatura
 * @property string $dataPagamento
 *
 * @property ImportacaoFatura $ImportacaoFatura
 * @property CirurgiaFatura[] $CirurgiaFaturas
 * 
 */
class Fatura extends ActiveRecord
{
    const TIPO_PGTO_CARTAO = "CAR";
    const TIPO_PGTO_DINHEIRO = "DIN";
    const TIPO_PGTO_CONVENIO = "CON";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fatura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['valorFatura', 'valorPago', 'idImportacaoFatura'], 'required'],
            [['valorFatura', 'valorGlosa', 'valorPago'], 'number'],
            [['idImportacaoFatura'], 'integer'],
            [['tipoPagamento'], 'string', 'max' => 100],
            [['tipoPagamento'], 'required', 'on'=>'view'],
            [['idImportacaoFatura'], 'exist', 'skipOnError' => true, 'targetClass' => ImportacaoFatura::className(), 'targetAttribute' => ['idImportacaoFatura' => 'idImportacaoFatura']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idFatura' => 'Id Fatura',
            'valorFatura' => 'Valor Fatura',
            'valorGlosa' => 'Valor Glosa',
            'valorPago' => 'Valor Pago',
            'tipoPagamento' => 'Tipo Pagamento',
            'idImportacaoFatura' => 'Importação Fatura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportacaoFatura()
    {
        return $this->hasOne(ImportacaoFatura::className(), ['idImportacaoFatura' => 'idImportacaoFatura']);
    }
    
    public function getCirurgiaFaturas()
    {
        return $this->hasMany(CirurgiaFatura::className(), ['idFatura' => 'idFatura']);
    }
    
    /**
     * @inheritdoc
     * @return FaturaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FaturaQuery(get_called_class());
    }
    
    public function importaFaturamento($caminhoArquivo, ImportacaoFatura $modelImportacao, $dataPagamento = null){
        if($modelImportacao->empresaFatura != "C"){
            $dadosImportar = Excel::widget([
                'mode' => 'import', 
                'fileName'=>$caminhoArquivo,
                'setFirstRecordAsKeys'=>false
            ]);
        }else{
            $str = mb_convert_encoding(file_get_contents($caminhoArquivo), "UTF-8");
            $json = Json::decode($str, false);
        }
        
        switch ($modelImportacao->empresaFatura){
            case "C":
                $this->importaJsonCoopanest($json, $modelImportacao, $dataPagamento);
                //$this->importaCoopanest($dadosImportar, $modelImportacao, $dataPagamento);
                break;
            case "U":
                $this->importaUnimed($dadosImportar, $modelImportacao);
                break;
            case "S-HMN":
            case "S-HCMF":
                //$this->importaBoletins($dadosImportar);
                $this->importaSantaCasa($dadosImportar, $modelImportacao);
                break;
        }
    }
    
    public function importaJsonCoopanest($json, ImportacaoFatura $modelImportacao, $dataPagamento){
        $modelCirurgia = new Cirurgias();
        $modelCirFat = new CirurgiaFatura();
        foreach ($json as $dados){            
            $boletim = $dados->boletim;
            $paciente = explode(" ", $dados->paciente);
            $data = date("d/m/Y", strtotime($dados->data));
            
            foreach ($dados->procedimentos as $procedimento){
                $cirurgia = null;
                
                $this->isNewRecord = true;
                unset($this->idFatura);

                $this->valorFatura = $procedimento->valorCobrado;
                $this->valorGlosa = $procedimento->valorGlosa;
                $this->valorPago = $procedimento->valorPago;
                $this->dataPagamento = $dataPagamento;
                $this->tipoPagamento = self::TIPO_PGTO_CONVENIO;

                $this->idImportacaoFatura = $modelImportacao->idImportacaoFatura;

                $procedimentos_a = Pacotes::find()->select('procedimento')->where(['pacote'=>$procedimento->codigo])->all();
                $procedimentos_array = [];

                foreach ($procedimentos_a as $p){
                    $procedimentos_array[] = $p->procedimento;
                }
//                if(!empty($procedimentos_array)){
//                    echo "<pre>";
//                    var_dump($procedimentos_array);
//                    exit();
//                }

                $cirurgiaFatura = $modelCirFat->buscaCirurgiaFatura($boletim, $procedimento->codigo, $data, $procedimentos_array);

                if($cirurgiaFatura == null){
                    $cirurgia = $modelCirurgia->find()->where([
                        'procedimento' => $procedimento->codigo,
                        'dataCirurgia' => DateTime::createFromFormat('d/m/Y', $data)->format('Y-m-d')
                    ])->andFilterWhere([
                        'like', 'nomePaciente', $paciente[0].'%', false
                    ])->andFilterWhere([
                        'like', 'nomePaciente', '%'.$paciente[count($paciente)-1], false
                    ])->one();

                    if($cirurgia == null){
                        $cirurgia = $modelCirurgia->find()->where([
                            'procedimento' => $procedimento->codigo,
                            'dataCirurgia' => DateTime::createFromFormat('d/m/Y', $data)->format('Y-m-d')
                        ])->andFilterWhere([
                            'like', 'nomePaciente', $paciente[0].'%', false
                        ])->one();
                    }

                    if($cirurgia != null){
                        $cirurgiaFatura = CirurgiaFatura::find()->where(["idCirurgias"=>$cirurgia->idCirurgias])->one();
                    }
                }
                
                if($cirurgiaFatura != null){
                    $this->save(false);

                    $cirurgiaFatura->idFatura = $this->idFatura;
                    $cirurgiaFatura->save();
                }
            }
        }
    }
    
    public function importaSantaCasa($dadosImportar, ImportacaoFatura $modelImportacao){
        $procedimento = "";
        $data = "";
        $atendimento = "";
        $idsCirurgias = [];
        $countAtendimento = [];
        $atendAux = "";
        $indice = 0;
        
        foreach ($dadosImportar as $index => $dados){
            if($index > 3){
                $data = $dados['E'];
                $procedimento = $dados['J'];

                if($dados['L'] == ""){
                    $descRepasse = explode("/", $dados["H"]);
                    $atendimento = (int)substr($descRepasse[0], -10);
                }else{
                    $atendimento = $dados['L'];
                }
                
                if($atendimento == "" || $atendimento <= 0){
                    $cirurgia = null;//Cirurgias::findOne(['atendimentoCirurgia'=>$atendimento]);
                }else{
                    $cirurgia = Cirurgias::findOne(['atendimentoCirurgia'=>$atendimento, "procedimento"=>$procedimento]);
                    if($cirurgia == null){
                        $procedimento_vinculado = ProcedimentosVinculados::find()->where(['procedimento_vinculado'=>$procedimento])->all();
                        if(count($procedimento_vinculado) == 1){
                            $cirurgia = Cirurgias::findOne(['atendimentoCirurgia'=>$atendimento, "procedimento"=>$procedimento_vinculado[0]->procedimento]);
                        }else if(count($procedimento_vinculado) > 1){
                            $arr_procedimentos = [];
                            foreach ($procedimento_vinculado as $proc){
                                $arr_procedimentos[] = $proc->procedimento;
                            }

                            $cirurgia = Cirurgias::find()->where(['atendimentoCirurgia'=>$atendimento])->andWhere(['in', "procedimento", $arr_procedimentos])->one();
                        }

                        if($cirurgia == null){
                            //$cirurgia = Cirurgias::find()->where(['atendimentoCirurgia'=>$atendimento])->andWhere(['not in', "idCirurgias", $idsCirurgias[$atendimento]])->one();
                            $cirurgiaAux = Cirurgias::find()->where(['atendimentoCirurgia'=>$atendimento])->all();
                            if($cirurgiaAux != null && $dados['L'] != ""){
                                if($atendAux == "" || $atendAux != $dados['L']){
                                    $indice = 0;
                                    $atendAux = $dados['L'];
                                }else{
                                    $indice++;
                                    $atendAux = $dados['L'];
                                }

                                if(count($cirurgiaAux)-1 >= $indice){
                                    $cirurgia = $cirurgiaAux[$indice];
                                }else{
                                    $cirurgia = $cirurgiaAux[0];
                                }
                            }else if($cirurgiaAux != null){
                                $cirurgia = $cirurgiaAux[0];
                            }
                        }
                    }
                }
                if($cirurgia != null){
                    $modelCirurgia = new CirurgiaFatura();
                    $modelCirurgia->isNewRecord = true;
                    unset($modelCirurgia->idCirurgiaFatura);
                    
                    $this->isNewRecord = true;
                    unset($this->idFatura);
                    
//                    $valor_pago = explode(",", $dados['N']);
//                    if(count($valor_pago) > 1 && $valor_pago[0] < 10){
//                        $valorPago = $valor_pago[0]."".$valor_pago[1];
//                    }else if(count($valor_pago) > 1 && $valor_pago[0] > 9){
//                        $valorPago = $valor_pago[0].".".$valor_pago[count($valor_pago)-1];
//                    }else{
//                        $valorPago = $valor_pago[0];
//                    }
                    
                    $valorPago = str_replace(",", ".", ($dados['L'] == "" ? $dados['I'] : $dados['N']));
                    
                    $this->valorFatura = $valorPago;
                    $this->valorGlosa = 0;
                    $this->valorPago = $valorPago;
                    $this->dataPagamento = $data;
                    $this->tipoPagamento = self::TIPO_PGTO_CONVENIO;
                    $this->idImportacaoFatura = $modelImportacao->idImportacaoFatura;
                    
                    $this->save(false);
                    
                    $desc = substr($dados["H"], 0, 120);
                    
                    $modelCirurgia->idFatura = $this->idFatura;
                    $modelCirurgia->idCirurgias = $cirurgia->idCirurgias;
                    $modelCirurgia->procedimento = $procedimento == "" ? $desc : $procedimento;
                    $modelCirurgia->empresaFatura = 'S';
                    $modelCirurgia->dataCirurgia = $cirurgia->dataCirurgia;
                    
                    $modelCirurgia->save();
                }
            }
        }
    }
    
    private function importaCoopanest($dadosImportar, ImportacaoFatura $modelImportacao, $dataPagamento){
        $modelCirurgia = new CirurgiaFatura();
        $podeInserir = false;
        $boletim = "";
        $procedimento = "";
        $data = "";
        // TODO: verificar atualização da informação quando for update
        foreach ($dadosImportar as $index => $dados){
            if(!$podeInserir){
                if(trim(strtoupper($dados['A'])) === "BOLETIM"){
                    $podeInserir = true;
                }
                if(ctype_digit($dados['A'])){
                    $podeInserir = true;
                    goto nextPoint;
                }
            }else{
                nextPoint:
                    
                $indiceBoletim = "A";
                $indiceData = "F";
                $indiceProfat = "G";
                $indiceDescProfat = "I";
                $indiceValorPago = "V";
                $indiceValorGlosa = "S";
                $indiceValorFatura = "P";
                
                if(!key_exists($indiceValorPago, $dados)){
                    $indiceValorPago = "R";
                    $indiceValorGlosa = "O";
                    $indiceValorFatura = "L";
                    $indiceData = "E";
                    $indiceProfat = "F";
                    $indiceDescProfat = "G";
                }
                if(is_null($dados[$indiceDescProfat]) && is_null($dados[$indiceData]) && is_null($dados[$indiceProfat])){
                    $podeInserir = false;
                }else{
                    $this->isNewRecord = true;
                    unset($this->idFatura);
                    
                    if(ctype_digit($dados[$indiceBoletim])){
                        $boletim = $dados[$indiceBoletim];
                        $data = $dados[$indiceData];
                        $valorGlosa = str_replace(",", "", $dados[$indiceValorGlosa])*-1;
                        $valorPago = str_replace(",", "", $dados[$indiceValorPago]) == "" ? 0 : str_replace(",", "", $dados[$indiceValorPago]);
                    }
                    
                    if(is_null($dados[$indiceValorFatura])){
                        continue;
                    }
                    
                    $valorFatura = str_replace(",", "", $dados[$indiceValorFatura]);
                    
                    $procedimento = $dados[$indiceProfat];
                    
                    $this->valorFatura = $valorFatura;
                    $this->valorGlosa = $valorGlosa;
                    $this->valorPago = $valorPago;
                    $this->dataPagamento = $dataPagamento;
                    
                    $this->idImportacaoFatura = $modelImportacao->idImportacaoFatura;
                        
                    $cirurgia = $modelCirurgia->buscaCirurgiaFatura($boletim, $procedimento, $data);
                    if($cirurgia != null){
                        $this->save(false);
                        
                        $valorGlosa = 0;
                        $valorPago = 0;

                        $cirurgia->idFatura = $this->idFatura;
                        $cirurgia->save();
                    }
                }
            }
        }
    }

    private function importaUnimed($dadosImportar, ImportacaoFatura $modelImportacao) {
        $modelCir = new Cirurgias();
        $modelCirurgia = new CirurgiaFatura();
        $podeInserir = false;
        
        if(key_exists(0, $dadosImportar)){
            $dadosImportar = $dadosImportar[0];
        }
        
        foreach ($dadosImportar as $index => $dados){
            if(!$podeInserir){
                if(strtoupper($dados['A']) == 'LOTE'){
                    $podeInserir = true;
                }
            }else{
                if($dados['A'] == ""){
                    $podeInserir = false;
                }else{
                    $nomeExploded = explode(" ", $dados['E']);
                    
                    $cirurgia = $modelCir->find()->where([
                        'procedimento' => str_pad($dados['G'], 8, "0", STR_PAD_LEFT),
                        'dataCirurgia' => explode(" ", $dados['D'])[0],
                        'nomeAnestesista'=>$dados['F']
                    ])->andFilterWhere([
                        'like', 'carteiraPaciente', $dados['C']
                    ])->andFilterWhere([
                        'like', 'nomePaciente', $nomeExploded[0].'%', false
                    ])->andFilterWhere([
                        'like', 'nomePaciente', '%'.$nomeExploded[count($nomeExploded)-1], false
                    ])->one();
                    
                    if($cirurgia == null){
                        $cirurgia = $modelCir->find()->where([
                            'procedimento' => str_pad($dados['G'], 8, "0", STR_PAD_LEFT),
                            'dataCirurgia' => explode(" ", $dados['D'])[0],
                            'nomeAnestesista'=>$dados['F']
                        ])->andFilterWhere([
                            'like', 'nomePaciente', $nomeExploded[0].'%', false
                        ])->one();
                    }
                    
                    if($cirurgia !== null){
                        $this->isNewRecord = true;
                        unset($this->idFatura);
                        
                        $this->valorFatura = str_replace(",", "", $dados['H']);
                        $this->valorPago = str_replace(",", "", $dados['H']);
                        $this->tipoPagamento = self::TIPO_PGTO_CONVENIO;
                        
                        $this->idImportacaoFatura = $modelImportacao->idImportacaoFatura;
                        
                        $this->save(false);
                        
                        $modelCirurgia->isNewRecord = true;
                        unset($modelCirurgia->idCirurgiaFatura);
                        
                        $modelCirurgia->dataCirurgia = $cirurgia->dataCirurgia;
                        $modelCirurgia->procedimento = $cirurgia->procedimento;
                        $modelCirurgia->empresaFatura = 'U';
                        $modelCirurgia->idCirurgias = $cirurgia->idCirurgias;
                        $modelCirurgia->idFatura = $this->idFatura;
                        
                        $modelCirurgia->save();
                    }
                }
            }
        }
    }
    
    public function importaBoletins($arquivo){
        $dadosErro = [];
        foreach ($arquivo as $index => $dados){
            //1115 - 5 colunas
            
            //1478 - Volta
            
            //1650 - 5colunas
            
            //1912 - volta
            
            //2005 - 5 colunas
            if($index > 1){
//                if($index >= 2 && $index <= 1114){
                    $atendimento = $dados['E'];
                    $boletim = $dados['B'];
//                }else if($index >= 1115 && $index <= 1477){
//                    $atendimento = $dados['E'];
//                    $boletim = $dados['B'];
//                }else if($index >= 1478 && $index <= 1649){
//                    $atendimento = $dados['D'];
//                    $boletim = $dados['A'];
//                }else if($index >= 1650 && $index <= 1911){
//                    $atendimento = $dados['E'];
//                    $boletim = $dados['B'];
//                }else if($index >= 1912 && $index <= 2004){
//                    $atendimento = $dados['D'];
//                    $boletim = $dados['A'];
//                }else if($index >= 2005){
//                    $atendimento = $dados['E'];
//                    $boletim = $dados['B'];
//                }
                
                $b1 = explode("/", $boletim);
                $b2 = explode(" ", $boletim);
                
                $cirurgias = Cirurgias::findAll(['atendimentoCirurgia'=>$atendimento]);
                if($cirurgias != null && count($b1) == 1 && count($b2) == 1){
                    $cirFatura = CirurgiaFatura::findAll(["boletim"=>$boletim]);
                    if($cirFatura == null){
                        foreach ($cirurgias as $c){
                            $modelBoletim = new CirurgiaFatura();
                            $modelBoletim->isNewRecord = true;
                            $modelBoletim->boletim = $boletim;
                            $modelBoletim->dataCirurgia = $c->dataCirurgia;
                            $modelBoletim->procedimento = $c->procedimento;
                            $modelBoletim->empresaFatura = 'C';
                            $modelBoletim->idCirurgias = $c->idCirurgias;
                            $modelBoletim->save();
                        }
                    }
                }else{
                    $dadosErro[] = $dados;
                }
            }
            
        }
        // Abre ou cria o arquivo bloco1.txt
        // "a" representa que o arquivo é aberto para ser escrito
        $fp = fopen("/home/erros.txt", "a");
        //$fp = fopen("/Users/lgmafra/erros.txt", "a");

        // Escreve "exemplo de escrita" no bloco1.txt
        $escreve = fwrite($fp, json_encode($dadosErro));

        // Fecha o arquivo
        fclose($fp);
    }

    public static function getTotal($provider, $column){
        $total = 0.00;
        
        foreach ($provider as $item){
            $total += $item[$column];
        }
        
        return number_format($total, 2, ".", "");
    }
}
