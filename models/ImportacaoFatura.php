<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\base\Exception;

/**
 * This is the model class for table "importacaoFatura".
 *
 * @property integer $idImportacaoFatura
 * @property string $competenciaFatura
 * @property string $descricao
 * @property string $empresaFatura
 * @property integer $idUsuario
 * @property string $arquivo
 * 
 * @property Fatura[] $Faturas
 * @property Usuario $Usuario
 */
class ImportacaoFatura extends ActiveRecord
{    
    public $_arquivoImportacao;
    public $dataPagamento;
    
    public function behaviors() {
        return [
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_AFTER_FIND=>[
                        'competenciaFatura'=>function(){}
                    ],
                ],
                'value'=>function($event){
                    $this->competenciaFatura = Yii::$app->formatter->asDate($this->competenciaFatura, "php:m/Y");
                    return $event;
                }
            ],
            [
                'class'=> AttributeBehavior::className(),
                'attributes'=>[
                    ActiveRecord::EVENT_BEFORE_INSERT=>[
                        'competenciaFatura'=>function(){},
                    ]
                ],
                'value'=>function($event){
                    $this->competenciaFatura = "01/".$this->competenciaFatura;
                    return $event;
                }
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'importacaoFatura';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['competenciaFatura', 'descricao', 'empresaFatura', 'idUsuario'], 'required'],
            [['competenciaFatura', 'dataPagamento'], 'safe'],
            [['idUsuario'], 'integer'],
            [['descricao'], 'string', 'max' => 100],
            [['_arquivoImportacao'], 'required'],
            //[['empresaFatura'], 'string', 'max' => 1],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'idUsuario']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idImportacaoFatura' => 'Id Importacao Fatura',
            'competenciaFatura' => 'Competência Fatura',
            'descricao' => 'Descrição',
            'empresaFatura' => 'Empresa',
            'idUsuario' => 'Usuário',
            'arquivo' => 'Arquivo Importação',
            '_arquivoImportacaoPgto'=>'Arquivo Importação Data Pagamento'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaturas()
    {
        return $this->hasMany(Fatura::className(), ['idImportacaoFatura' => 'idImportacaoFatura']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['idUsuario' => 'idUsuario']);
    }

    /**
     * @inheritdoc
     * @return ImportacaoFaturaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImportacaoFaturaQuery(get_called_class());
    }
    
    public function getPathFile(){
        return Yii::getAlias("@webroot")."/uploads/";
    }
    
    public function getNameFile($extension){
        $comp = explode("/", $this->competenciaFatura);
        return date("Y").date("m").date("H").date("i")."_fatura_".$comp[1]."_".$comp[0].".".$extension;
    }
    
    public function validaCompetenciaImportada(){
        if(ImportacaoFatura::find()->where(['competenciaFatura'=>'01/'.$this->competenciaFatura, 'empresaFatura'=>$this->empresaFatura])->exists()){
            throw new Exception("Fatura já cadastrada. Escolha outra competência.");
        }
    }
}
