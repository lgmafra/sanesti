<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "prestador_mv".
 *
 * @property int $idPrestadorMV
 * @property int $codigoPrestador
 * @property string $nomePrestador
 * @property string $crmPrestador
 */
class PrestadorMv extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'prestador_mv';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigoPrestador', 'nomePrestador', 'crmPrestador'], 'required'],
            [['codigoPrestador'], 'default', 'value' => null],
            [['codigoPrestador'], 'integer'],
            [['nomePrestador'], 'string', 'max' => 150],
            [['crmPrestador'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idPrestadorMV' => 'Id Prestador Mv',
            'codigoPrestador' => 'Codigo Prestador',
            'nomePrestador' => 'Nome Prestador',
            'crmPrestador' => 'Crm Prestador',
        ];
    }

    /**
     * @inheritdoc
     * @return PrestadorMvQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrestadorMvQuery(get_called_class());
    }
}
