/**
 *  JS default, tudo que for comum a varios sistemas entrará neste JS
 */
$(document).ready(function () {
    $('.data').mask('00/00/0000');
    $('.competencia').mask('00/0000');
    $('.hora').mask('00:00');
    $('.chapa').mask('000000');
    $('.year').mask('0000');
    // Mask de campos editaveis
    $('.money-mask').mask('000.000,00', {reverse: true});
    // Mask de campos não editaveis
    $('.money').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    $('.money-bold').css('font-weight', 'bold').priceFormat({
        prefix: 'R$ ',
        centsSeparator: ',',
        thousandsSeparator: '.'
    });
    // Busca generica dentro dos modais
    $(".busca_in_modal").keyup(function (e) {
        $('.modal-list table tbody').find('tr').each(function () {
            var textoTable = $(this).find('.nome').text();
            var valueTable = $(this).find('.codigo').text();
            var value = valueTable.indexOf(e.target.value) >= 0;
            var textoLower = textoTable.toLowerCase().indexOf(e.target.value) >= 0;
            var textoUpper = textoTable.toUpperCase().indexOf(e.target.value) >= 0;
            $(this).css('display', textoLower || textoUpper || value ? '' : 'none');
        });
    });
});